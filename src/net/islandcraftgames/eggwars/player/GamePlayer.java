package net.islandcraftgames.eggwars.player;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.gameapi.IGamePlayer;
import net.islandcraftgames.eggwars.game.Game;
import net.islandcraftgames.eggwars.kits.KitsMain;
import net.islandcraftgames.eggwars.storage.DataStorage;

public class GamePlayer extends IGamePlayer{

	private final UUID uuid;
	private final Player bukkitPlayer;
	private final String playerName;
	private Game game;
	private int gamesPlayed;
	private int gamesWon;
	private int kills;
	private int deaths;
	private boolean skipFallDamage;
	private boolean skipFireTicks;
	private ItemStack[] savedInventoryContents = null;
	private ItemStack[] savedArmorContents = null;
	private boolean[] kits;

	public GamePlayer(UUID uuid) {
		this.uuid = uuid;
		this.bukkitPlayer = Bukkit.getPlayer(this.uuid);
		this.playerName = bukkitPlayer.getName();
		DataStorage.get().loadPlayer(this);
	}

	public void save() {
		DataStorage.get().savePlayer(this);
	}

	public UUID getUUID() {
		return this.uuid;
	}
	
	public Game getGame(){
		return this.game;
	}
	
	public boolean isPlaying(){
		return this.game != null;
	}
	
	public void setGame(Game game){
		this.game = game;
	}

	public Player getBukkitPlayer() {
		return this.bukkitPlayer;
		}

	public String toString()
	{
		return this.playerName;
		}

	
	public String getName() {
		return this.playerName;
		}

	
	public int getGamesPlayed() {
		return this.gamesPlayed;
		}

	
	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
		}

	
	public int getGamesWon() {
		return this.gamesWon;
		}

	
	public void setGamesWon(int gamesWon) {
		this.gamesWon = gamesWon;
		}

	
	public int getKills() {
		return this.kills;
		}

	
	public void setKills(int kills) {
		this.kills = kills;
		}

	
	public int getDeaths() {
		return this.deaths;
		}

	
	public void setDeaths(int deaths) {
		this.deaths = deaths;
		}

	
	public void setSkipFallDamage(boolean skipFallDamage) {
		this.skipFallDamage = skipFallDamage;
		}

	
	public void setSkipFireTicks(boolean skipFireTicks) {
		this.skipFireTicks = skipFireTicks;
		}

	
	public boolean shouldSkipFallDamage() {
		return this.skipFallDamage;
		}

	
	public boolean shouldSkipFireTicks() {
		return this.skipFireTicks;
		}

	
	public void saveCurrentState() {
		this.savedArmorContents = ((ItemStack[]) this.bukkitPlayer
				.getInventory().getArmorContents().clone());
		this.savedInventoryContents = ((ItemStack[]) this.bukkitPlayer
				.getInventory().getContents().clone());
		}

	
	public void restoreState()
	{
		boolean shouldUpdateInventory = false;
		
		if (this.savedArmorContents != null) {
			this.bukkitPlayer.getInventory().setArmorContents(
					this.savedArmorContents);
			this.savedArmorContents = null;
			shouldUpdateInventory = true;
			}
		
		if (this.savedInventoryContents != null) {
			this.bukkitPlayer.getInventory().setContents(
					this.savedInventoryContents);
			this.savedInventoryContents = null;
			shouldUpdateInventory = true;
			}
		
		if (shouldUpdateInventory) {
			this.bukkitPlayer.updateInventory();
			}
		}

	public void setKits(boolean[] kits) {
		if (kits.length != KitsMain.getAllKits().size())
			return;
		this.kits = kits;
	}

	public boolean[] getKits() {
		return this.kits;
	}
}