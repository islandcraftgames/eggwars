package net.islandcraftgames.eggwars.kits.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.gameapi.IKit;

public class Fall extends IKit {

	public Fall() {
		setID(0);
		setLangCode("ctf.kit0");
		setItems(new ItemStack[] {});
		setShowCaseItem(new ItemStack(Material.IRON_BOOTS, 1));
		setPrice(2000);
		setBeforeKit(null);
		setSlot(2);
		setPermission("");
	}

}
