package net.islandcraftgames.eggwars.kits.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.gameapi.IKit;

public class BombermanExtreme extends IKit {

	public BombermanExtreme() {
		setID(3);
		setLangCode("ctf.kit3");
		setItems(new ItemStack[] {new ItemStack(Material.TNT, 4)});
		setShowCaseItem(new ItemStack(Material.TNT, 5));
		setPrice(6000);
		setBeforeKit(new Bomberman());
		setSlot(8);
		setPermission("vip++");
	}
	
}
