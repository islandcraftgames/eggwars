package net.islandcraftgames.eggwars.kits.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.gameapi.IKit;

public class Blocks extends IKit {

	public Blocks() {
		setID(1);
		setLangCode("ctf.kit1");
		setItems(new ItemStack[] { new ItemStack(Material.COBBLESTONE, 64) });
		setShowCaseItem(new ItemStack(Material.COBBLESTONE, 64));
		setPrice(1500);
		setBeforeKit(null);
		setSlot(6);
		setPermission("");
	}

}
