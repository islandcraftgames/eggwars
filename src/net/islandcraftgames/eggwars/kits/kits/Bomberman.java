package net.islandcraftgames.eggwars.kits.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.gameapi.IKit;

public class Bomberman extends IKit {

	public Bomberman() {
		setID(2);
		setLangCode("ctf.kit2");
		setItems(new ItemStack[] {new ItemStack(Material.TNT, 1)});
		setShowCaseItem(new ItemStack(Material.TNT, 1));
		setPrice(3000);
		setBeforeKit(null);
		setSlot(0);
		setPermission("vip");
	}
	
}
