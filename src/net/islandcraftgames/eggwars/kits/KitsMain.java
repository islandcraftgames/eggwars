package net.islandcraftgames.eggwars.kits;

import java.util.List;

import com.google.common.collect.Lists;

import me.islandcraft.gameapi.IKit;
import net.islandcraftgames.eggwars.kits.kits.Blocks;
import net.islandcraftgames.eggwars.kits.kits.Bomberman;
import net.islandcraftgames.eggwars.kits.kits.BombermanExtreme;
import net.islandcraftgames.eggwars.kits.kits.Fall;

public class KitsMain {

	private static List<IKit> kitList = Lists.newArrayList();
	private static boolean settedup = false;

	public static List<IKit> getAllKits() {
		if (!settedup)
			setup();
		return kitList;
	}

	public static void setup() {
		if (settedup)
			return;

		kitList.add(new Fall());
		kitList.add(new Blocks());
		kitList.add(new Bomberman());
		kitList.add(new BombermanExtreme());

		settedup = true;
	}
}
