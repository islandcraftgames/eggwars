package net.islandcraftgames.eggwars.storage;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nonnull;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.islandcraft.mainhub.Main;
import net.islandcraftgames.eggwars.kits.KitsMain;
import net.islandcraftgames.eggwars.player.GamePlayer;

public class FlatFileStorage extends DataStorage {
	public void loadPlayer(@Nonnull GamePlayer player) {
		try {
			File dataDirectory = Main.get().getDataFolder();
			File playerDataDirectory = new File(dataDirectory, "players");

			if ((!playerDataDirectory.exists())
					&& (!playerDataDirectory.mkdirs())) {
				System.out.println("Failed to load player " + player
						+ ": Could not create player_data directory.");
				return;
			}

			File playerFile = new File(playerDataDirectory, player.getUUID()
					.toString() + ".yml");
			if ((!playerFile.exists()) && (!playerFile.createNewFile())) {
				System.out.println("Failed to load player " + player
						+ ": Could not create player file.");
				return;
			}

			FileConfiguration fileConfiguration = YamlConfiguration
					.loadConfiguration(playerFile);

			player.setGamesWon(fileConfiguration.getInt("capturetheflag.wins", 0));
			player.setGamesPlayed(fileConfiguration.getInt("capturetheflag.played", 0));
			player.setKills(fileConfiguration.getInt("capturetheflag.kills", 0));
			player.setDeaths(fileConfiguration.getInt("capturetheflag.deaths", 0));
			boolean[] kits = new boolean[KitsMain.getAllKits().size()];
			for (int i = 0; i < KitsMain.getAllKits().size(); i++) {
				kits[i] = fileConfiguration.getBoolean("capturetheflag.k"
						+ (i + 1), false);
			}
			player.setKits(kits);
		} catch (IOException ioException) {
			System.out.println("Failed to load player " + player + ": "
					+ ioException.getMessage());
		}
	}

	public void savePlayer(@Nonnull GamePlayer player) {
		try {
			File dataDirectory = Main.get().getDataFolder();
			File playerDataDirectory = new File(dataDirectory, "players");

			if ((!playerDataDirectory.exists())
					&& (!playerDataDirectory.mkdirs())) {
				System.out.println("Failed to save player " + player
						+ ": Could not create player_data directory.");
				return;
			}

			File playerFile = new File(playerDataDirectory, player.getUUID()
					.toString() + ".yml");
			if ((!playerFile.exists()) && (!playerFile.createNewFile())) {
				System.out.println("Failed to save player " + player
						+ ": Could not create player file.");
				return;
			}

			FileConfiguration fileConfiguration = YamlConfiguration
					.loadConfiguration(playerFile);
			fileConfiguration
					.set("capturetheflag.wins", Integer.valueOf(player.getGamesWon()));
			fileConfiguration.set("capturetheflag.played",
					Integer.valueOf(player.getGamesPlayed()));
			fileConfiguration
					.set("capturetheflag.deaths", Integer.valueOf(player.getDeaths()));
			fileConfiguration.set("capturetheflag.kills", Integer.valueOf(player.getKills()));
			boolean[] kits = player.getKits();
			for(int i = 0; i < kits.length; i++){
				fileConfiguration.set("capturetheflag.k" + (i+1), kits[i]);
			}
			fileConfiguration.save(playerFile);
		} catch (IOException ioException) {
			System.out.println("Failed to save player " + player + ": "
					+ ioException.getMessage());
		}
	}
}