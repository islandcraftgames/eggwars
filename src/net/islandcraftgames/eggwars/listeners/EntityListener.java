package net.islandcraftgames.eggwars.listeners;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import me.islandcraft.gameapi.IGameState;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.controllers.TNTController;
import net.islandcraftgames.eggwars.game.Game;
import net.islandcraftgames.eggwars.player.GamePlayer;

public class EntityListener implements org.bukkit.event.Listener {

	@EventHandler(ignoreCancelled = true)
	public void onEntityDamage(EntityDamageEvent event) {
		if (event.getEntityType() != EntityType.PLAYER) {
			return;
		}

		Player player = (Player) event.getEntity();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if (!gamePlayer.isPlaying()) {
			return;
		}

		Game game = gamePlayer.getGame();

		if (game.getState() != IGameState.PLAYING) {
			event.setCancelled(true);
			return;
		}

		if (game.getSpawningPlayers().contains(player)) {
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onTeamDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {

			Player p = (Player) e.getEntity();
			GamePlayer gp = PlayerController.get().get(p);
			Player k = (Player) e.getDamager();
			GamePlayer gk = PlayerController.get().get(k);

			if (!(gp.isPlaying() && gk.isPlaying())) {
				return;
			}

			Game pGame = gp.getGame();
			Game kGame = gk.getGame();

			if (pGame != kGame) {
				return;
			}

			if (pGame.getPlayerTeam(gp) == kGame.getPlayerTeam(gk)) {
				e.setCancelled(true);
				return;
			}
		} else if (e.getEntity() instanceof Player && e.getDamager() instanceof Arrow) {

			Arrow arrow = ((Arrow) e.getDamager());

			arrow.remove();

			if (!(arrow.getShooter() instanceof Player)) {
				return;
			}

			Player p2 = (Player) e.getEntity();
			GamePlayer gp2 = PlayerController.get().get(p2);
			Player k2 = (Player) arrow.getShooter();
			GamePlayer gk2 = PlayerController.get().get(k2);

			if (!(gp2.isPlaying() && gk2.isPlaying())) {
				return;
			}

			Game pGame2 = gp2.getGame();
			Game kGame2 = gk2.getGame();

			if (pGame2 != kGame2) {
				return;
			}

			if (pGame2.getPlayerTeam(gp2) == kGame2.getPlayerTeam(gk2)) {
				e.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if (!gamePlayer.isPlaying())
			return;

		gamePlayer.getGame().onPlayerDeath(gamePlayer, event);
	}

	@SuppressWarnings("rawtypes")
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e) {
		if (e.getEntityType() != EntityType.PRIMED_TNT)
			return;
		TNTPrimed tnt = (TNTPrimed) e.getEntity();
		UUID uuid = tnt.getUniqueId();
		Game game = TNTController.getTNTGame(uuid);
		if (game == null)
			return;
		List destroyed = e.blockList();
		Iterator it = destroyed.iterator();
		while (it.hasNext()) {
			Block b = (Block) it.next();
			if (!game.isPlacedBlock(b))
				it.remove();
		}
		TNTController.removeTNT(uuid);
	}

}