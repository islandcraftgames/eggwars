package net.islandcraftgames.eggwars.listeners;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.IKit;
import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.build.VillagerKits;
import net.islandcraftgames.eggwars.config.PluginConfig;
import net.islandcraftgames.eggwars.controllers.GameController;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.game.Game;
import net.islandcraftgames.eggwars.kits.KitsMain;
import net.islandcraftgames.eggwars.player.GamePlayer;

public class VillagerListener implements Listener {

	@EventHandler
	public void onVillagerInteract(EntityInteractEvent e) {
		if (e.getEntity() instanceof Villager) {
			for (IGame g : GameController.get().getAll())
				if (((Game) g).isVillager((Villager) e.getEntity())) {
					e.setCancelled(true);
					return;
				}
			if (!PluginConfig.isKitsOn())
				return;
			File directory = EggWars.get().getDataFolder();
			File villagersFile = new File(directory, "villagers.yml");

			if (!villagersFile.exists()) {
				return;
			}

			YamlConfiguration villagers = new YamlConfiguration();

			try {
				villagers.load(villagersFile);

				if (villagers.getString("UUID").equalsIgnoreCase(e.getEntity().getUniqueId().toString())) {
					e.setCancelled(true);
				}

			} catch (IOException | InvalidConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onVillagerDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Villager) {
			for (IGame g : GameController.get().getAll())
				if (((Game) g).isVillager((Villager) e.getEntity())) {
					e.setCancelled(true);
					return;
				}
			if (!PluginConfig.isKitsOn())
				return;
			File directory = EggWars.get().getDataFolder();
			File villagersFile = new File(directory, "villagers.yml");

			if (!villagersFile.exists()) {
				return;
			}

			YamlConfiguration villagers = new YamlConfiguration();

			try {
				villagers.load(villagersFile);

				if (villagers.getString("UUID").equalsIgnoreCase(e.getEntity().getUniqueId().toString())) {
					e.setCancelled(true);
				}

			} catch (IOException | InvalidConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onPlayerClick(PlayerInteractEntityEvent e) {
		if (!PluginConfig.isKitsOn())
			return;
		if (e.getRightClicked() instanceof Villager) {
			File directory = EggWars.get().getDataFolder();
			File villagersFile = new File(directory, "villagers.yml");

			if (!villagersFile.exists()) {
				return;
			}

			YamlConfiguration villagers = new YamlConfiguration();

			try {
				villagers.load(villagersFile);

				if (villagers.getString("UUID").equalsIgnoreCase(e.getRightClicked().getUniqueId().toString())) {
					VillagerKits.openGUI(e.getPlayer());
					e.setCancelled(true);
				}

			} catch (IOException | InvalidConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onEat(PlayerItemConsumeEvent e) {
		GamePlayer gp = PlayerController.get().get(e.getPlayer());
		if (gp.isPlaying()) {
			if (gp.getGame().getState() == IGameState.WAITING) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		GamePlayer gp = PlayerController.get().get(e.getPlayer());
		if (gp.isPlaying()) {
			if (gp.getGame().getState() == IGameState.WAITING) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		if (!PluginConfig.isKitsOn())
			return;
		if (ChatColor.stripColor(e.getInventory().getName()).equalsIgnoreCase(EggWars.getGameName())) {
			e.setCancelled(true);
			Player p = (Player) e.getWhoClicked();
			GamePlayer gp = PlayerController.get().get((Player) e.getWhoClicked());
			boolean[] kits = gp.getKits();
			List<IKit> a = KitsMain.getAllKits();
			for (IKit kit : a) {
				if (e.getSlot() == kit.getSlot() && p.hasPermission(kit.getPerm())) {
					if (kits[kit.getID()]) {
						gp.getBukkitPlayer().sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.alreadykit",
								new String[] { LanguageMain.get(p, kit.getLangCode()) }));
						gp.getBukkitPlayer().closeInventory();
						return;
					}
					if (kit.getBeforeKit() != null) {
						if (!kits[kit.getBeforeKit().getID()]) {
							gp.getBukkitPlayer().sendMessage(ChatColor.RED + LanguageMain.get(p, "sw.needkit",
									new String[] { LanguageMain.get(p, kit.getBeforeKit().getLangCode()) }));
							gp.getBukkitPlayer().closeInventory();
							return;
						}
					}
					if (Main.getEconomy().withdrawPlayer(gp.getName(), kit.getPrice())) {
						kits[kit.getID()] = true;
						gp.setKits(kits);
						gp.getBukkitPlayer()
								.sendMessage(ChatColor.GREEN
										+ LanguageMain.get(p, "sw.buykit",
												new String[] { LanguageMain.get(p, kit.getLangCode()) })
										+ ChatColor.GOLD + Main.getEconomy().getBalance(gp.getName()));
						gp.getBukkitPlayer().closeInventory();
					} else {
						gp.getBukkitPlayer()
								.sendMessage(
										ChatColor.RED
												+ LanguageMain
														.get(p, "sw.nomoneykit",
																new String[] {
																		Double.toString(kit.getPrice() - Main
																				.getEconomy().getBalance(gp.getName())),
																		LanguageMain.get(p, kit.getLangCode()) }));
						gp.getBukkitPlayer().closeInventory();
					}
					gp.getBukkitPlayer().closeInventory();
				}
			}
		}
	}
}
