package net.islandcraftgames.eggwars.listeners;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.util.Vector;

import com.google.common.collect.Maps;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGamePlayer;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.ITeam;
import me.islandcraft.gameapi.controllers.ChatController;
import me.islandcraft.mainhub.events.PlayerSpeakEvent;
import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.utils.LocationUtils;
import me.islandcraft.mainhub.utils.StringUtils;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUIMain;
import net.islandcraftgames.eggwars.controllers.GameController;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.game.Game;
import net.islandcraftgames.eggwars.generators.Generator;
import net.islandcraftgames.eggwars.player.GamePlayer;

public class PlayerListener implements org.bukkit.event.Listener {
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if (gamePlayer.isPlaying()) {
			gamePlayer.getGame().onPlayerLeave(gamePlayer);
		}

		gamePlayer.save();
		PlayerController.get().unregister(player);
	}

	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);

		if (gamePlayer.isPlaying()) {
			String command = event.getMessage().split(" ")[0].toLowerCase();

			if (!(command.equalsIgnoreCase("/" + EggWars.getGameAlias())
					|| command.equalsIgnoreCase("/" + EggWars.getGameName()) || command.equalsIgnoreCase("/leave"))
					|| command.equalsIgnoreCase("/radio") || command.equalsIgnoreCase("/music")) {
				event.setCancelled(true);
				player.sendMessage(ChatColor.RED + LanguageMain.get(player, "sw.nocmdarena"));
			}
		}
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		Location from = e.getFrom();
		Location to = e.getTo();
		if ((from.getBlockX() == to.getBlockX()) && (from.getBlockY() == to.getBlockY())
				&& (from.getBlockZ() == to.getBlockZ())) {
			return;
		}
		Player p = e.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(p);
		if (!gamePlayer.isPlaying()) {
			return;
		}

		if (gamePlayer.getGame().getSpawningPlayers().contains(p)) {
			gamePlayer.getGame().getSpawningPlayers().remove(p);
		}

		if (to.getWorld() != gamePlayer.getGame().getWorld())
			return;
		Vector minVec = gamePlayer.getGame().getMinVec();
		Vector maxVec = gamePlayer.getGame().getMaxVec();
		if (!to.toVector().isInAABB(minVec, maxVec)) {
			p.sendMessage(ChatColor.RED + "Voc� n�o pode sair da arena!");
			p.teleport(from);
		}
	}

	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent e) {
		Player p = e.getPlayer();
		GamePlayer gP = PlayerController.get().get(p);
		if (gP == null) {
			return;
		}
		if (!gP.isPlaying()) {
			return;
		}
		Vector minVec = gP.getGame().getMinVec();
		Vector maxVec = gP.getGame().getMaxVec();
		if (e.getTo().getWorld() != gP.getGame().getWorld()) {
			p.sendMessage(ChatColor.RED + "Voc� saiu da arena!");
			gP.getGame().onPlayerLeave(gP);
		} else if (!e.getTo().toVector().isInAABB(minVec, maxVec)) {
			p.sendMessage(ChatColor.RED + "Voc� saiu da arena!");
			gP.getGame().onPlayerLeave(gP);
		}
	}

	@EventHandler
	public void onPlayerFlight(PlayerToggleFlightEvent e) {
		GamePlayer gP = PlayerController.get().get(e.getPlayer());
		if (gP == null) {
			return;
		}
		if (!gP.isPlaying()) {
			return;
		}
		if (e.isFlying()) {
			e.setCancelled(true);
			e.getPlayer().setAllowFlight(false);
			e.getPlayer().setFlying(false);
			e.getPlayer().sendMessage(ChatColor.RED + "N�o � permitido voar enquanto jogar!");
		}
	}

	@EventHandler
	public void onGameModeChange(PlayerGameModeChangeEvent e) {
		GamePlayer gP = PlayerController.get().get(e.getPlayer());
		if (gP == null) {
			return;
		}
		if (!gP.isPlaying()) {
			return;
		}
		if (!e.getNewGameMode().equals(GameMode.SURVIVAL)) {
			e.setCancelled(true);
			e.getPlayer().setGameMode(GameMode.SURVIVAL);
			e.getPlayer().sendMessage(ChatColor.RED + "N�o � permitido mudar de gamemode enquanto joga!");
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		PlayerController.get().register(e.getPlayer());
		if (GameController.get().getAll().size() != 0 && GameController.get().getAll() != null)
			for (IGame game : GameController.get().getAll()) {
				for (IGamePlayer gp : game.getPlayers()) {
					gp.getBukkitPlayer().hidePlayer(e.getPlayer());
				}
			}
	}

	@EventHandler
	public void onTeleportToInGame(PlayerTeleportEvent e) {
		if (!e.getPlayer().getName().equalsIgnoreCase("Rexcantor64"))
			if (e.getTo().getWorld().getName().startsWith(EggWars.getGameAlias() + "-"))
				if (!e.getFrom().getWorld().getName().startsWith(EggWars.getGameAlias() + "-"))
					if (!PlayerController.get().get(e.getPlayer()).isPlaying())
						e.setCancelled(true);

	}

	@EventHandler
	public void onTeamChange(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			GamePlayer gp = PlayerController.get().get(e.getPlayer());
			if (e.getItem() == null)
				return;
			if (gp.isPlaying())
				if (e.getItem().hasItemMeta())
					if (e.getItem().getType() == Material.WOOL)
						if (gp.getGame().getState() == IGameState.WAITING) {
							if (e.getItem().getDurability() == 11)
								gp.getGame().changeTeam(gp, ITeam.BLUE);
							if (e.getItem().getDurability() == 14)
								gp.getGame().changeTeam(gp, ITeam.RED);
							if (e.getItem().getDurability() == 5)
								gp.getGame().changeTeam(gp, ITeam.GREEN);
							if (e.getItem().getDurability() == 4)
								gp.getGame().changeTeam(gp, ITeam.YELLOW);
						}

		}
	}

	@EventHandler
	public void onEggDestroyed(PlayerInteractEvent e) {
		if (e.getAction() != Action.LEFT_CLICK_BLOCK && e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		Player p = e.getPlayer();
		GamePlayer gp = PlayerController.get().get(p);
		if (!gp.isPlaying())
			return;
		Game game = gp.getGame();
		Block b = e.getClickedBlock();
		if (b.getType() != Material.DRAGON_EGG)
			return;
		Location l = b.getLocation();
		Location[] locs = game.getSpawnPlaces();
		e.setCancelled(true);
		if (game.getState() != IGameState.PLAYING)
			return;
		ITeam team = ITeam.NEUTRAL;
		if (LocationUtils.isBlockEquals(locs[5], l)) {
			team = ITeam.BLUE;
		} else if (LocationUtils.isBlockEquals(locs[6], l)) {
			team = ITeam.RED;
		} else if (LocationUtils.isBlockEquals(locs[7], l)) {
			team = ITeam.YELLOW;
		} else if (LocationUtils.isBlockEquals(locs[8], l)) {
			team = ITeam.GREEN;
		} else {
			e.setCancelled(false);
			return;
		}
		if (game.getPlayerTeam(gp) != team)
			game.brokeEgg(team, l, gp);
		else
			gp.getBukkitPlayer().sendMessage(ChatColor.RED + "You can't break your own egg!");

	}

	@EventHandler
	public void onVillagerInteract(PlayerInteractEntityEvent e) {
		if (!(e.getRightClicked() instanceof Villager))
			return;
		for (IGame g : GameController.get().getAll())
			if (((Game) g).isVillager((Villager) e.getRightClicked())) {
				e.setCancelled(true);
				Inventory inv = Bukkit.createInventory(null, 9, ChatColor.YELLOW + "EggWars InGame Shop");
				for (MerchantGUI gui : MerchantGUIMain.getAllGUIs())
					inv.setItem(gui.getID(), gui.getShowcaseItem(e.getPlayer()));
				e.getPlayer().openInventory(inv);
			}
	}

	@EventHandler
	public void onVillagerGUIClick(final InventoryClickEvent e) {
		if (e.getClickedInventory() == null)
			return;
		if (e.getClickedInventory().getTitle() == null)
			return;
		if (!ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase("EggWars InGame Shop"))
			return;
		e.setCancelled(true);
		final MerchantGUI gui = MerchantGUIMain.getGUI((Player) e.getWhoClicked(), e.getCurrentItem().getType());
		if (gui != null) {
			e.getWhoClicked().closeInventory();
			Bukkit.getScheduler().runTaskLater(EggWars.get(), new Runnable() {

				@Override
				public void run() {
					gui.openGUI((Player) e.getWhoClicked());
				}
			}, 1L);
		}
	}

	private HashMap<Player, Generator> playerGenMap = Maps.newHashMap();

	@EventHandler
	public void onGeneratorClick(PlayerInteractEvent e) {
		if (e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		if (!e.getClickedBlock().getWorld().getName().contains(EggWars.getGameAlias()))
			return;
		for (IGame game : GameController.get().getAll())
			if (game.getState() == IGameState.PLAYING)
				for (Generator g : ((Game) game).getGenerators()) {
					if (LocationUtils.isBlockEquals(g.getLocation(false), e.getClickedBlock().getLocation())
							|| LocationUtils.isBlockEquals(g.getSignLocation(false),
									e.getClickedBlock().getLocation())) {
						if (g.getCurrentLevel() == 0) {
							Inventory inv = Bukkit.createInventory(null, 27,
									ChatColor.GOLD + g.getType().getName() + " Generator - Broken");
							inv.setItem(11, ItemsMain.createItem(Material.PAPER, 1,
									ChatColor.GOLD + g.getType().getName() + " Generator", new String[] { "Broken" }));
							inv.setItem(15,
									ItemsMain
											.createItem(g.getMaterial(), 1,
													ChatColor.GOLD
															+ "Repair Generator",
													new String[] {
															"Repair cost: " + g.getNextGLevel().getCost() + " x "
																	+ StringUtils.formatString(
																			g.getNextGLevel().getCostItem().toString()),
															"Time to spawn a ingot after repair: "
																	+ g.getNextGLevel().getTime() + " seconds" }));
							e.getPlayer().openInventory(inv);
						} else {
							Inventory inv = Bukkit.createInventory(null, 27, ChatColor.GOLD + g.getType().getName()
									+ " Generator - Level " + g.getCurrentLevel());
							inv.setItem(11, ItemsMain.createItem(Material.PAPER, 1,
									ChatColor.GOLD + g.getType().getName() + " Generator",
									new String[] { "Level: " + g.getCurrentLevel(),
											"Time to spawn a ingot: " + g.getCurrentGLevel().getTime() + " seconds" }));
							if (g.hasNextLevel())
								inv.setItem(15,
										ItemsMain.createItem(g.getMaterial(), 1,
												ChatColor.GOLD + "Upgrade to level "
														+ (g.getCurrentLevel() + 1),
												new String[] {
														"Upgrade cost: " + g.getNextGLevel().getCost() + " x "
																+ StringUtils.formatString(
																		g.getNextGLevel().getCostItem().toString()),
														"Time to spawn a ingot after upgrade: "
																+ g.getNextGLevel().getTime() + " seconds" }));
							else
								inv.setItem(15, ItemsMain.createItem(Material.OBSIDIAN, 1,
										ChatColor.GOLD + "Generator has reached maximum level!"));
							e.getPlayer().openInventory(inv);
						}
						playerGenMap.put(e.getPlayer(), g);
						return;
					}
				}
	}

	@EventHandler
	public void onGeneratorGUIClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (!p.getWorld().getName().startsWith(EggWars.getGameAlias()))
			return;
		if (!playerGenMap.containsKey(p))
			return;
		Generator gen = playerGenMap.get(p);
		Inventory inv = e.getClickedInventory();
		if (inv.getTitle() == null)
			return;
		if (!ChatColor.stripColor(inv.getTitle()).contains(" Generator - "))
			return;
		e.setCancelled(true);
		if (e.getRawSlot() != 15)
			return;
		if (inv.getItem(15).getType() == Material.OBSIDIAN)
			return;
		gen.upgrade(p);
		p.closeInventory();
	}

	@EventHandler
	public void onGeneratorGUIClose(InventoryCloseEvent e) {
		if (playerGenMap.containsKey(e.getPlayer()))
			playerGenMap.remove(e.getPlayer());
	}

	@EventHandler
	public void onChat(PlayerSpeakEvent e) {
		ChatController.sendChatGame(e, EggWars.getGameType(), EggWars.getGameName());
	}
}