package net.islandcraftgames.eggwars.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.ITeamGame;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.controllers.GameController;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.controllers.TNTController;
import net.islandcraftgames.eggwars.game.Game;
import net.islandcraftgames.eggwars.player.GamePlayer;
import net.islandcraftgames.eggwars.storage.SignStorage;

public class BlockListener implements org.bukkit.event.Listener {
	@EventHandler(ignoreCancelled = true)
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);
		ITeamGame game = (ITeamGame) gamePlayer.getGame();

		if ((gamePlayer.isPlaying()) && (game.getState() == IGameState.WAITING)) {
			event.setCancelled(true);
			return;
		}

		if (gamePlayer.isPlaying()) {
			if (isDeny(game.getSpawnPlaces()[0], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}
			if (isDeny(game.getSpawnPlaces()[1], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}
			if (isDeny(game.getSpawnPlaces()[2], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}
			if (isDeny(game.getSpawnPlaces()[3], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}

			if (event.getBlock().getType() == Material.TNT) {
				event.getBlock().setType(Material.AIR);
				TNTController.addTNT(
						event.getBlock().getLocation().getWorld().spawnEntity(
								event.getBlock().getLocation().clone().add(0.5D, 0.5D, 0.5D), EntityType.PRIMED_TNT),
						(Game) game);
				return;
			}

			((Game) game).addPlacedBlock(event.getBlock());
		}
	}

	private boolean isDeny(Location l, Location toCheck) {
		World w = l.getWorld();
		int x = l.getBlockX();
		int y = l.getBlockY();
		int z = l.getBlockZ();
		Location max = new Location(w, x + 1, y + 2, z + 1);
		Location min = new Location(w, x - 1, y - 1, z - 1);
		return isInside(toCheck, new Location[] { min, max });
	}

	private boolean isInside(Location toCheck, Location[] minMax) {
		Location min = minMax[0];
		Location max = minMax[1];
		World w = min.getWorld();

		if (max.getWorld() != w) {
			return false;
		}

		for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
			for (int y = min.getBlockY(); y <= max.getBlockY(); y++) {
				for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
					if (equalsLoc(toCheck, new Location(w, x, y, z))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean equalsLoc(Location l, Location l2) {
		World w = l.getWorld();
		int x = l.getBlockX();
		int y = l.getBlockY();
		int z = l.getBlockZ();
		World w2 = l2.getWorld();
		int x2 = l2.getBlockX();
		int y2 = l2.getBlockY();
		int z2 = l2.getBlockZ();

		if (w == w2 && x == x2 && y == y2 && z == z2)
			return true;
		return false;
	}

	@EventHandler
	public void noBlockGlitching(BlockPlaceEvent e) {
		if (e.isCancelled()) {
			Location l1 = e.getPlayer().getLocation();
			Location l2 = e.getBlock().getLocation();
			int x1 = l1.getBlockX();
			int y1 = l1.getBlockY() - 1;
			int z1 = l1.getBlockZ();
			int x2 = l2.getBlockX();
			int y2 = l2.getBlockY();
			int z2 = l2.getBlockZ();
			if (x1 == x2 && y1 == y2 && z1 == z2) {
				e.getPlayer().teleport(l1.clone().add(0, -1, 0));
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		GamePlayer gamePlayer = PlayerController.get().get(player);
		ITeamGame game = (ITeamGame) gamePlayer.getGame();

		if ((gamePlayer.isPlaying()) && (game.getState() == IGameState.WAITING)) {
			event.setCancelled(true);
			return;
		}

		if (gamePlayer.isPlaying()) {

			if (!((Game) game).isPlacedBlock(event.getBlock())) {
				event.setCancelled(true);
				return;
			}

			if (isDeny(game.getSpawnPlaces()[0], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}
			if (isDeny(game.getSpawnPlaces()[1], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}
			if (isDeny(game.getSpawnPlaces()[2], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}
			if (isDeny(game.getSpawnPlaces()[3], event.getBlock().getLocation())) {
				event.setCancelled(true);
				return;
			}

			((Game) game).removePlacedBlock(event.getBlock());
		}

		if (SignStorage.isSign(event.getBlock().getLocation())) {
			SignStorage.removeSign(event.getBlock().getLocation());
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onSignCreate(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("[" + EggWars.getGameAlias() + "]") && e.getLine(1) != "") {
			e.setLine(0, ChatColor.BLUE + "[" + EggWars.getGameName() + "]");
			SignStorage.addSign(e.getBlock().getLocation(), e.getLine(1));
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onSignInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (SignStorage.isSign(e.getClickedBlock().getLocation())) {
				if (e.getClickedBlock().getType() == Material.SIGN
						|| e.getClickedBlock().getType() == Material.SIGN_POST
						|| e.getClickedBlock().getType() == Material.WALL_SIGN) {
					Sign sign = (Sign) e.getClickedBlock().getState();
					e.setCancelled(true);
					GamePlayer gp = PlayerController.get().get(e.getPlayer());
					if (gp.isPlaying())
						return;
					Party party = PartyManager.getPlayerParty(e.getPlayer());
					if (party != null) {
						if (party.isOwner(e.getPlayer())) {
							int size = party.getSize();
							Game game = GameController.get().findEmpty(sign.getLine(1), size);
							for (Player p : party.getPlayers()) {
								game.onPlayerLogin(PlayerController.get().get(p));
							}
						} else {
							e.getPlayer().sendMessage(
									ChatColor.RED + LanguageMain.get(e.getPlayer(), "party.onlyownercanjoin"));
						}
					} else {
						Game game = GameController.get().findEmpty(sign.getLine(1), 1);
						game.onPlayerLogin(gp);
					}
				}
			}
		}
	}

	@EventHandler
	public void noIceMelt(BlockFadeEvent e) {
		String wn = e.getBlock().getWorld().getName();
		if (!wn.matches(EggWars.getGameAlias() + "-\\d+")) {
			return;
		}
		Material type = e.getBlock().getType();
		if (type == Material.ICE || type == Material.SNOW || type == Material.SNOW_BLOCK) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void noIceForm(BlockFormEvent e) {
		String wn = e.getBlock().getWorld().getName();
		if (!wn.matches(EggWars.getGameAlias() + "-\\d+")) {
			return;
		}
		Material type = e.getBlock().getType();
		if (type == Material.ICE || type == Material.SNOW || type == Material.SNOW_BLOCK) {
			e.setCancelled(true);
		}
	}
}