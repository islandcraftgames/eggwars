package net.islandcraftgames.eggwars.build;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import me.islandcraft.gameapi.IKit;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.kits.KitsMain;
import net.islandcraftgames.eggwars.player.GamePlayer;

public class VillagerKits {

	public static void openGUI(GamePlayer p) {
		boolean[] kits = p.getKits();
		Inventory inv = Bukkit.createInventory(null, EggWars.getShopSize(), ChatColor.YELLOW
				+ EggWars.getGameName());
		List<IKit> a = KitsMain.getAllKits();
		for (IKit kit : a) {
			if (p.getBukkitPlayer().hasPermission(kit.getPerm())) {
				ItemStack is = kit.getShowCaseItem();
				ItemMeta im = is.getItemMeta();
				if (kits[kit.getID()]) {
					im.setDisplayName(ChatColor.RED
							+ LanguageMain.get(p.getBukkitPlayer(),
									kit.getLangCode()));
				} else {
					im.setDisplayName(ChatColor.GREEN
							+ LanguageMain.get(p.getBukkitPlayer(),
									kit.getLangCode()));
				}
				List<String> lore = Lists.newArrayList();
				lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + kit.getPrice()
						+ "$");
				im.setLore(lore);
				is.setItemMeta(im);
				inv.setItem(kit.getSlot(), is);
			}
		}
		p.getBukkitPlayer().openInventory(inv);
	}

	public static void openGUI(Player p) {
		openGUI(PlayerController.get().get(p));
	}

}
