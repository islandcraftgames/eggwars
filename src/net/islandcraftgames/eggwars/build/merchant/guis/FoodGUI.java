package net.islandcraftgames.eggwars.build.merchant.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.Merchants;

public class FoodGUI extends MerchantGUI {

	@Override
	public void openGUI(Player p) {
		MerchantAPI api = Merchants.get();
		Merchant merchant = api.newMerchant(ChatColor.GOLD + LanguageMain.get(p, "ew.food"));

		/* Offers */
		merchant.addOffer(api.newOffer(new ItemStack(Material.BAKED_POTATO, 3), new ItemStack(Material.IRON_INGOT, 2)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.COOKED_BEEF, 1), new ItemStack(Material.IRON_INGOT, 5)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.CAKE), new ItemStack(Material.IRON_INGOT, 5)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.BAKED_POTATO), new ItemStack(Material.IRON_INGOT, 3)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.GOLDEN_APPLE), new ItemStack(Material.GOLD_INGOT, 2)));
		merchant.addOffer(api.newOffer(ItemsMain.createItem(Material.GOLDEN_APPLE, 1, 1, ""), new ItemStack(Material.DIAMOND, 32)));		
		/* End Offers */

		merchant.addCustomer(p);
	}
	
	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemsMain.createItem(Material.COOKED_BEEF, 1, ChatColor.GOLD + LanguageMain.get(p, "ew.food"));
	}

	@Override
	public int getID() {
		return 3;
	}

}
