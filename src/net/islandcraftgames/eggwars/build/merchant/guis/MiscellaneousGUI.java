package net.islandcraftgames.eggwars.build.merchant.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.Merchants;

public class MiscellaneousGUI extends MerchantGUI {

	@Override
	public void openGUI(Player p) {
		MerchantAPI api = Merchants.get();
		Merchant merchant = api.newMerchant(LanguageMain.get(p, "ew.miscellaneous"));

		/* Offers */
		merchant.addOffer(api.newOffer(new ItemStack(Material.CHEST, 1), new ItemStack(Material.GOLD_INGOT, 3)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.TNT, 1), new ItemStack(Material.GOLD_INGOT, 5)));
		/* End Offers */

		merchant.addCustomer(p);
	}
	
	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemsMain.createItem(Material.CHEST, 1, ChatColor.GOLD + LanguageMain.get(p, "ew.miscellaneous"));
	}

	@Override
	public int getID() {
		return 6;
	}

}
