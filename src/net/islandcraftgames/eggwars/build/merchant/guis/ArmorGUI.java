package net.islandcraftgames.eggwars.build.merchant.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.Merchants;

public class ArmorGUI extends MerchantGUI {

	@Override
	public void openGUI(Player p) {
		MerchantAPI api = Merchants.get();
		Merchant merchant = api.newMerchant(ChatColor.GOLD + LanguageMain.get(p, "ew.armor"));

		/* Offers */
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.LEATHER_HELMET, 1), new ItemStack(Material.IRON_INGOT, 2)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.LEATHER_CHESTPLATE, 1), new ItemStack(Material.IRON_INGOT, 2)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.LEATHER_LEGGINGS, 1), new ItemStack(Material.IRON_INGOT, 2)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.LEATHER_BOOTS, 1), new ItemStack(Material.IRON_INGOT, 2)));

		merchant.addOffer(
				api.newOffer(new ItemStack(Material.CHAINMAIL_HELMET, 1), new ItemStack(Material.GOLD_INGOT, 2)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1), new ItemStack(Material.GOLD_INGOT, 2)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1), new ItemStack(Material.GOLD_INGOT, 2)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.CHAINMAIL_BOOTS, 1), new ItemStack(Material.GOLD_INGOT, 2)));

		merchant.addOffer(api.newOffer(new ItemStack(Material.IRON_HELMET, 1), new ItemStack(Material.DIAMOND, 5)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.IRON_CHESTPLATE, 1), new ItemStack(Material.DIAMOND, 5)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.IRON_LEGGINGS, 1), new ItemStack(Material.DIAMOND, 5)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.IRON_BOOTS, 1), new ItemStack(Material.DIAMOND, 5)));

		merchant.addOffer(api.newOffer(new ItemStack(Material.DIAMOND_HELMET, 1), new ItemStack(Material.DIAMOND, 32)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.DIAMOND_CHESTPLATE, 1), new ItemStack(Material.DIAMOND, 32)));
		merchant.addOffer(
				api.newOffer(new ItemStack(Material.DIAMOND_LEGGINGS, 1), new ItemStack(Material.DIAMOND, 32)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.DIAMOND_BOOTS, 1), new ItemStack(Material.DIAMOND, 32)));
		/* End Offers */

		merchant.addCustomer(p);
	}

	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemsMain.createItem(Material.IRON_CHESTPLATE, 1, ChatColor.GOLD + LanguageMain.get(p, "ew.armor"));
	}

	@Override
	public int getID() {
		return 2;
	}

}
