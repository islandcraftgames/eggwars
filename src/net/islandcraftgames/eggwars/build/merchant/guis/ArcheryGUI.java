package net.islandcraftgames.eggwars.build.merchant.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.Merchants;

public class ArcheryGUI extends MerchantGUI {

	@Override
	public void openGUI(Player p) {
		MerchantAPI api = Merchants.get();
		Merchant merchant = api.newMerchant(ChatColor.GOLD + LanguageMain.get(p, "ew.archery"));

		/* Offers */
		merchant.addOffer(api.newOffer(new ItemStack(Material.BOW, 1), new ItemStack(Material.DIAMOND, 6)));
		merchant.addOffer(api.newOffer(
				ItemsMain.createItem(Material.BOW, 1, "",
						new Object[][] { new Object[] { Enchantment.ARROW_DAMAGE, 1 } }),
				new ItemStack(Material.DIAMOND, 12)));
		merchant.addOffer(api.newOffer(
				ItemsMain.createItem(Material.BOW, 1, "",
						new Object[][] { new Object[] { Enchantment.ARROW_DAMAGE, 2 } }),
				new ItemStack(Material.DIAMOND, 24)));
		merchant.addOffer(api.newOffer(
				ItemsMain.createItem(Material.BOW, 1, "",
						new Object[][] { new Object[] { Enchantment.ARROW_KNOCKBACK, 1 } }),
				new ItemStack(Material.DIAMOND, 16)));
		merchant.addOffer(api.newOffer(
				ItemsMain.createItem(Material.BOW, 1, "",
						new Object[][] { new Object[] { Enchantment.ARROW_KNOCKBACK, 2 } }),
				new ItemStack(Material.DIAMOND, 32)));
		merchant.addOffer(api.newOffer(
				ItemsMain.createItem(Material.BOW, 1, "",
						new Object[][] { new Object[] { Enchantment.ARROW_DAMAGE, 2 },
								new Object[] { Enchantment.ARROW_KNOCKBACK, 2 } }),
				new ItemStack(Material.DIAMOND, 64)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.ARROW, 5), new ItemStack(Material.GOLD_INGOT, 2)));
		/* End Offers */

		merchant.addCustomer(p);
	}

	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemsMain.createItem(Material.BOW, 1, ChatColor.GOLD + LanguageMain.get(p, "ew.archery"));
	}

	@Override
	public int getID() {
		return 5;
	}

}
