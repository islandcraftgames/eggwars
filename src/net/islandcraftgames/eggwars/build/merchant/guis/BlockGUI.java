package net.islandcraftgames.eggwars.build.merchant.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.Merchants;

public class BlockGUI extends MerchantGUI {

	@Override
	public void openGUI(Player p) {
		MerchantAPI api = Merchants.get();
		Merchant merchant = api.newMerchant(ChatColor.GOLD + LanguageMain.get(p, "ew.blocks"));

		/* Offers */
		merchant.addOffer(api.newOffer(new ItemStack(Material.STONE, 2), new ItemStack(Material.IRON_INGOT, 1)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.GLASS, 5), new ItemStack(Material.IRON_INGOT, 3)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.OBSIDIAN), new ItemStack(Material.GOLD_INGOT, 5)));
		/* End Offers */

		merchant.addCustomer(p);
	}
	
	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemsMain.createItem(Material.BRICK, 1, ChatColor.GOLD + LanguageMain.get(p, "ew.blocks"));
	}

	@Override
	public int getID() {
		return 0;
	}

}
