package net.islandcraftgames.eggwars.build.merchant.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.Merchants;

public class ToolsGUI extends MerchantGUI {

	@Override
	public void openGUI(Player p) {
		MerchantAPI api = Merchants.get();
		Merchant merchant = api.newMerchant(ChatColor.GOLD + LanguageMain.get(p, "ew.tools"));

		/* Offers */
		merchant.addOffer(api.newOffer(new ItemStack(Material.STONE_PICKAXE, 1), new ItemStack(Material.IRON_INGOT, 10)));
		merchant.addOffer(api.newOffer(new ItemStack(Material.IRON_PICKAXE, 1), new ItemStack(Material.GOLD_INGOT, 10)));
		merchant.addOffer(api.newOffer(ItemsMain.createItem(Material.DIAMOND_PICKAXE, 1, "",
				new Object[][] { new Object[] { Enchantment.DIG_SPEED, 1 } }), new ItemStack(Material.GOLD_INGOT, 32)));

		merchant.addOffer(api.newOffer(ItemsMain.createItem(Material.DIAMOND_PICKAXE, 1, "",
				new Object[][] { new Object[] { Enchantment.DIG_SPEED, 3 } }), new ItemStack(Material.DIAMOND, 15)));
		/* End Offers */

		merchant.addCustomer(p);
	}
	
	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemsMain.createItem(Material.DIAMOND_PICKAXE, 1, ChatColor.GOLD + LanguageMain.get(p, "ew.tools"));
	}

	@Override
	public int getID() {
		return 4;
	}

}
