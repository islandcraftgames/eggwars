package net.islandcraftgames.eggwars.build.merchant.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.build.merchant.MerchantGUI;
import net.islandcraftgames.merchantapi.api.Merchant;
import net.islandcraftgames.merchantapi.api.MerchantAPI;
import net.islandcraftgames.merchantapi.api.Merchants;

public class CombatGUI extends MerchantGUI {

	@Override
	public void openGUI(Player p) {
		MerchantAPI api = Merchants.get();
		Merchant merchant = api.newMerchant(ChatColor.GOLD + LanguageMain.get(p, "ew.combat"));

		/* Offers */
		merchant.addOffer(api.newOffer(new ItemStack(Material.WOOD_SWORD, 1), new ItemStack(Material.IRON_INGOT, 2)));
		
		merchant.addOffer(api.newOffer(new ItemStack(Material.STONE_SWORD, 1), new ItemStack(Material.IRON_INGOT, 16)));
		
		merchant.addOffer(api.newOffer(new ItemStack(Material.IRON_SWORD, 1), new ItemStack(Material.GOLD_INGOT, 32)));
		
		merchant.addOffer(api.newOffer(new ItemStack(Material.DIAMOND_SWORD, 1), new ItemStack(Material.DIAMOND, 16)));
		/* End Offers */

		merchant.addCustomer(p);
	}
	
	@Override
	public ItemStack getShowcaseItem(Player p) {
		return ItemsMain.createItem(Material.DIAMOND_SWORD, 1, ChatColor.GOLD + LanguageMain.get(p, "ew.combat"));
	}

	@Override
	public int getID() {
		return 1;
	}

}
