package net.islandcraftgames.eggwars.build.merchant;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class MerchantGUI {
	
	public abstract void openGUI(Player p);
	
	public abstract ItemStack getShowcaseItem(Player p);
	
	public abstract int getID();
	
}
