package net.islandcraftgames.eggwars.build.merchant;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import net.islandcraftgames.eggwars.build.merchant.guis.ArcheryGUI;
import net.islandcraftgames.eggwars.build.merchant.guis.ArmorGUI;
import net.islandcraftgames.eggwars.build.merchant.guis.BlockGUI;
import net.islandcraftgames.eggwars.build.merchant.guis.CombatGUI;
import net.islandcraftgames.eggwars.build.merchant.guis.FoodGUI;
import net.islandcraftgames.eggwars.build.merchant.guis.MiscellaneousGUI;
import net.islandcraftgames.eggwars.build.merchant.guis.ToolsGUI;

public class MerchantGUIMain {

	private static List<MerchantGUI> guiList = Lists.newArrayList();
	private static boolean settedup = false;

	public static List<MerchantGUI> getAllGUIs() {
		if (!settedup)
			setup();
		return guiList;
	}

	public static void setup() {
		if (settedup)
			return;
		
		guiList.add(new ArcheryGUI());
		guiList.add(new ArmorGUI());
		guiList.add(new BlockGUI());
		guiList.add(new CombatGUI());
		guiList.add(new FoodGUI());
		guiList.add(new MiscellaneousGUI());
		guiList.add(new ToolsGUI());

		settedup = true;
	}

	public static MerchantGUI getGUI(Player p, Material m) {
		if (!settedup)
			setup();
		for (MerchantGUI gui : getAllGUIs())
			if (gui.getShowcaseItem(p).getType() == m)
				return gui;
		return null;
	}

}
