package net.islandcraftgames.eggwars.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nonnull;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.inventivetalent.bossbar.BossBarAPI;
import org.primesoft.asyncworldedit.blockPlacer.BlockPlacerPlayer;
import org.primesoft.asyncworldedit.playerManager.PlayerEntry;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sk89q.worldedit.CuboidClipboard;

import me.islandcraft.gameapi.IGamePlayer;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.gameapi.ITeam;
import me.islandcraft.gameapi.ITeamGame;
import me.islandcraft.gameapi.IWEUtils;
import me.islandcraft.gameapi.Spectable;
import me.islandcraft.mainhub.items.ItemsMain;
import me.islandcraft.mainhub.language.LanguageMain;
import me.islandcraft.mainhub.managers.SbManager;
import me.islandcraft.mainhub.packetManagers.Title;
import me.islandcraft.mainhub.player.IslandPlayer;
import me.islandcraft.mainhub.player.PlayerManager;
import me.islandcraft.mainhub.utils.EntityUtils;
import me.islandcraft.mainhub.utils.StringUtils;
import me.islandcraft.mainhub.utils.TagUtils;
import me.islandcraft.party.Party;
import me.islandcraft.party.PartyManager;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.config.PluginConfig;
import net.islandcraftgames.eggwars.controllers.GameController;
import net.islandcraftgames.eggwars.controllers.KitManager;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.controllers.WorldController;
import net.islandcraftgames.eggwars.generators.Generator;
import net.islandcraftgames.eggwars.generators.types.DiamondGenerator;
import net.islandcraftgames.eggwars.generators.types.GoldGenerator;
import net.islandcraftgames.eggwars.generators.types.IronGenerator;
import net.islandcraftgames.eggwars.player.GamePlayer;
import net.islandcraftgames.eggwars.utilities.CraftBukkitUtil;
import net.islandcraftgames.eggwars.utilities.GeneratorUtils;
import net.islandcraftgames.eggwars.utilities.PlayerUtil;

@SuppressWarnings("deprecation")
public class Game extends ITeamGame implements Spectable {

	private IGameState gameState;
	private Map<GamePlayer, ITeam> playerTeamMap = Maps.newHashMap();
	private Map<GamePlayer, Integer> playerDeathsMap = Maps.newHashMap();
	private Map<GamePlayer, Integer> playerKillsMap = Maps.newHashMap();
	private Map<ITeam, Boolean> eggMap = Maps.newHashMap();

	private int playerCount = 0;
	private int slots;
	private Location[] spawnPlaces;

	private List<Player> spawning = Lists.newArrayList();
	private List<GamePlayer> spectators = Lists.newArrayList();
	private List<GamePlayer> toJoin = Lists.newArrayList();
	private List<Generator> generators = Lists.newArrayList();
	private List<Location> toMakeGenerators = Lists.newArrayList();
	private List<Block> placedBlocks = Lists.newArrayList();

	private int timer;
	private boolean built;
	private CuboidClipboard schematic;
	private World world;
	private int[] islandReference;
	private org.bukkit.util.Vector minVec;
	private org.bukkit.util.Vector maxVec;

	private PlayerEntry pe;
	private BlockPlacerPlayer bpp;
	private IWEUtils weutil;

	private Map<Villager, Location> villagers = Maps.newHashMap();
	private List<Location> toSpawnVillagers = Lists.newArrayList();

	public Game(CuboidClipboard schematic) {
		this.built = false;
		this.schematic = schematic;
		this.world = WorldController.get().create(this, schematic);
		this.slots = 24;
		this.gameState = IGameState.WAITING;
		this.timer = 60;
	}

	public boolean isBuilt() {
		return this.built;
	}

	public CuboidClipboard getSchematic() {
		return this.schematic;
	}

	public void setBuilt(boolean built) {
		if (built == true) {
			setTimer(61);
			if (toJoin.size() != 0)
				for (GamePlayer gp : toJoin) {
					if (BossBarAPI.hasBar(gp.getBukkitPlayer()))
						BossBarAPI.removeBar(gp.getBukkitPlayer());
					onPlayerJoin(gp);
				}
			toJoin.clear();
			final IWEUtils a = this.weutil;
			Bukkit.getScheduler().runTaskLater(EggWars.get(), new Runnable() {
				@Override
				public void run() {
					a.shutdown();
				}
			}, 1L);
			setEggBlock(ITeam.BLUE, this.spawnPlaces[5]);
			setEggBlock(ITeam.RED, this.spawnPlaces[6]);
			setEggBlock(ITeam.YELLOW, this.spawnPlaces[7]);
			setEggBlock(ITeam.GREEN, this.spawnPlaces[8]);
			setupVillagers();
			setupGenerators();
		}
		this.built = built;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public boolean isReady() {
		return this.slots >= 2;
	}

	public World getWorld() {
		return this.world;
	}

	public void setGridReference(int[] gridReference) {
		this.islandReference = gridReference;
	}

	public int[] getGridReference() {
		return this.islandReference;
	}

	public int getSlots() {
		return this.slots;
	}

	public void setPlayerEntry(PlayerEntry pe) {
		this.pe = pe;
	}

	public PlayerEntry getPlayerEntry() {
		return this.pe;
	}

	public void setBlockPlacerPlayer(BlockPlacerPlayer bpp) {
		this.bpp = bpp;
	}

	public BlockPlacerPlayer getBlockPlacerPlayer() {
		return this.bpp;
	}

	public void setLocation(int midX, int midZ) {
		int minX = midX + this.schematic.getOffset().getBlockX() + 1;
		int minZ = midZ + this.schematic.getOffset().getBlockZ() + 1;
		int maxX = minX + this.schematic.getWidth() - 2;
		int maxZ = minZ + this.schematic.getLength() - 2;
		int buffer = PluginConfig.getIslandBuffer();
		this.minVec = new org.bukkit.util.Vector(minX - buffer, Integer.MIN_VALUE, minZ - buffer);
		this.maxVec = new org.bukkit.util.Vector(maxX + buffer, Integer.MAX_VALUE, maxZ + buffer);
	}

	public org.bukkit.util.Vector getMinVec() {
		return this.minVec;
	}

	public org.bukkit.util.Vector getMaxVec() {
		return this.maxVec;
	}

	public void onPlayerLogin(GamePlayer gp) {
		Player player = gp.getBukkitPlayer();
		gp.setGame(this);
		if (!isBuilt()) {
			player.sendMessage(LanguageMain.get(player, "game.building"));
			toJoin.add(gp);
			return;
		} else {
			onPlayerJoin(gp);
		}

	}

	public void onPlayerJoin(GamePlayer gamePlayer) {
		Player player = gamePlayer.getBukkitPlayer();
		gamePlayer.setGame(this);

		this.playerTeamMap.put(gamePlayer, ITeam.NEUTRAL);

		this.playerCount++;

		sendMessage("sw.joinarena", new String[] { player.getDisplayName(), getPlayerCount() + "", this.slots + "" });

		PlayerUtil.refreshPlayer(player);

		if (PluginConfig.clearInventory()) {
			PlayerUtil.clearInventory(player);
		}

		if (player.getGameMode() != org.bukkit.GameMode.SURVIVAL) {
			player.setGameMode(org.bukkit.GameMode.SURVIVAL);
		}

		Plugin commandBook = EggWars.get().getServer().getPluginManager().getPlugin("CommandBook");
		Plugin worldGuard = EggWars.get().getServer().getPluginManager().getPlugin("WorldGuard");
		if (player.hasMetadata("god")) {
			if ((commandBook != null) && ((commandBook instanceof com.sk89q.commandbook.CommandBook))) {
				player.removeMetadata("god", commandBook);
			}
			if ((worldGuard != null) && ((worldGuard instanceof com.sk89q.worldguard.bukkit.WorldGuardPlugin))) {
				player.removeMetadata("god", worldGuard);
			}
		}

		playerDeathsMap.put(gamePlayer, 0);
		playerKillsMap.put(gamePlayer, 0);

		registerScoreboard(gamePlayer, true);

		player.teleport(this.spawnPlaces[4].clone().add(0, 5, 0));

		setInvTeams(player);

		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!this.playerTeamMap.containsKey(PlayerController.get().get(p)))
				player.hidePlayer(p);
		}
		for (IGamePlayer p : getPlayers()) {
			p.getBukkitPlayer().showPlayer(player);
		}
	}

	public void onPlayerLeave(IGamePlayer gamePlayer) {
		onPlayerLeave((GamePlayer) gamePlayer, false, true, false);
	}

	public void onPlayerLeave(GamePlayer gamePlayer, boolean finish, boolean eliminate, boolean forcenoleft) {

		Player player = gamePlayer.getBukkitPlayer();

		if (isBuilt())
			this.playerCount--;

		if (this.toJoin.contains(gamePlayer)) {
			this.toJoin.remove(gamePlayer);
		}

		if (!finish) {
			if (!forcenoleft)
				sendMessage("sw.left", new String[] { player.getDisplayName(), Integer.toString(getPlayerCount()),
						Integer.toString(this.slots) });
		}

		if (eliminate && forcenoleft && this.gameState == IGameState.PLAYING) {
			sendMessage("ew.eliminate", new String[] { player.getName() });
		}

		TagUtils.setPrefix(gamePlayer.getBukkitPlayer(), "", getPlayers());
		for (IGamePlayer gp : getPlayers()) {
			TagUtils.setPrefix(gp.getBukkitPlayer(), "",
					new ArrayList<IGamePlayer>(Arrays.asList(new IGamePlayer[] { gamePlayer })));
		}

		this.playerTeamMap.remove(gamePlayer);
		this.playerKillsMap.remove(gamePlayer);
		this.playerDeathsMap.remove(gamePlayer);

		registerScoreboardAll(false);

		if (!finish && gameState == IGameState.PLAYING) {
			List<ITeam> aliveTeams = getAliveTeams();
			if (aliveTeams.size() == 1) {
				onGameEnd(aliveTeams.get(0));
			}
		}

		player.setGameMode(GameMode.SURVIVAL);
		player.setAllowFlight(false);
		player.setFlying(false);
		gamePlayer.setGame(null);
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p != player) {
				player.showPlayer(p);
			}
		}
		for (IGamePlayer p : getPlayers()) {
			p.getBukkitPlayer().hidePlayer(player);
		}
		ItemsMain.setStartItems(player, false);
		PlayerUtil.refreshPlayer(player);
		SbManager.updateScoreboard(player);

		player.teleport(PluginConfig.getLobbySpawn());

	}

	public void onPlayerDeath(GamePlayer gamePlayer, PlayerDeathEvent event) {
		Player player = gamePlayer.getBukkitPlayer();
		Player killer = player.getKiller();

		playerDeathsMap.put(gamePlayer, playerDeathsMap.get(gamePlayer) + 1);

		if (killer != null) {
			GamePlayer gameKiller = PlayerController.get().get(killer);
			playerKillsMap.put(gameKiller, playerKillsMap.get(gameKiller) + 1);
			sendMessage("ctf.die", new String[] { player.getDisplayName(), killer.getDisplayName() });
		} else {
			sendMessage("ctf.suicide", new String[] { player.getDisplayName() });
		}

		if (event != null) {
			event.setDeathMessage(null);
			event.getDrops().clear();
			event.setDroppedExp(0);

		}

		if (player.isDead()) {
			CraftBukkitUtil.forceRespawn(player);
		}
		final GamePlayer gp2 = gamePlayer;
		if (hasEgg(getPlayerTeam(gamePlayer)))
			Bukkit.getScheduler().runTaskLater(EggWars.get(), new Runnable() {

				@Override
				public void run() {
					onPlayerRespawn(gp2);
					registerScoreboardAll(false);
				}
			}, 2L);
		else {
			onPlayerLeave(gamePlayer, false, true, true);
			gamePlayer.getBukkitPlayer().sendMessage(ChatColor.RED
					+ "Your egg is broken and you can't respawn. The spectator module is currently in maintenance!");
		}
	}

	public void onPlayerRespawn(GamePlayer gp) {
		final Player p = gp.getBukkitPlayer();
		p.setFireTicks(0);
		KitManager.getKits(gp);
		p.updateInventory();
		Location l;
		switch (this.playerTeamMap.get(gp)) {
		default:
			l = spawnPlaces[0];
			break;
		case RED:
			l = spawnPlaces[1];
			break;
		case YELLOW:
			l = spawnPlaces[2];
			break;
		case GREEN:
			l = spawnPlaces[3];
			break;
		}
		p.teleport(l);
		PlayerUtil.refreshPlayer(p);

		spawning.add(p);

		Bukkit.getScheduler().runTaskLater(EggWars.get(), new Runnable() {

			@Override
			public void run() {
				if (spawning.contains(p)) {
					spawning.remove(p);
				}
			}
		}, 100L);
	}

	public void onGameStart() {
		this.gameState = IGameState.PLAYING;

		equilibrateTeams();

		for (Villager v : villagers.keySet())
			v.teleport(villagers.get(v));

		for (GamePlayer gp : this.playerTeamMap.keySet()) {

			if (gp != null) {

				KitManager.getKits(gp);
				switch (this.playerTeamMap.get(gp)) {
				default:
					gp.getBukkitPlayer().teleport(spawnPlaces[0]);
					TagUtils.setPrefix(gp.getBukkitPlayer(), ChatColor.BLUE + "", getPlayers());
					break;
				case RED:
					gp.getBukkitPlayer().teleport(spawnPlaces[1]);
					TagUtils.setPrefix(gp.getBukkitPlayer(), ChatColor.RED + "", getPlayers());
					break;
				case YELLOW:
					gp.getBukkitPlayer().teleport(spawnPlaces[2]);
					TagUtils.setPrefix(gp.getBukkitPlayer(), ChatColor.YELLOW + "", getPlayers());
					break;
				case GREEN:
					gp.getBukkitPlayer().teleport(spawnPlaces[3]);
					TagUtils.setPrefix(gp.getBukkitPlayer(), ChatColor.GREEN + "", getPlayers());
					break;
				}
				gp.setGamesPlayed(gp.getGamesPlayed() + 1);
				registerScoreboard(gp, false);
			}

			PlayerUtil.clearInventory(gp.getBukkitPlayer());
			PlayerUtil.refreshPlayer(gp.getBukkitPlayer());

			gp.getBukkitPlayer().sendMessage(ChatColor.GOLD + LanguageMain.get(gp.getBukkitPlayer(), "sw.start"));
		}
	}

	public void onGameEnd() {
		onGameEnd(ITeam.NEUTRAL);
	}

	public void onGameEnd(@Nonnull ITeam team) {
		this.gameState = IGameState.ENDING;

		if (team != ITeam.NEUTRAL) {
			for (GamePlayer gp : getTeamPlayers(team)) {
				gp.setGamesWon(gp.getGamesWon() + 1);
			}
			sendMessage((team == ITeam.BLUE ? ChatColor.BLUE + "" : ChatColor.RED + "") + "A equipa "
					+ team.toString().toLowerCase() + " ganhou!");
		} else {
			sendMessage(ChatColor.GOLD, "pw.tie", new String[] {});
		}

		for (IGamePlayer igp : getPlayers()) {
			GamePlayer gp = (GamePlayer) igp;
			Player p = gp.getBukkitPlayer();
			int credits = 0;
			credits += (this.playerKillsMap.get(gp) * PluginConfig.getScorePerKill(gp.getBukkitPlayer()));
			if (getPlayerTeam(gp) == team && team != ITeam.NEUTRAL)
				credits += PluginConfig.getScorePerWin(gp.getBukkitPlayer());
			EggWars.getEconomy().depositPlayer(p.getName(), credits);
			p.sendMessage(ChatColor.GOLD + "------------------------");
			p.sendMessage(
					ChatColor.GOLD + LanguageMain.get(p, "sw.getcredits", new String[] { Integer.toString(credits) }));
			p.sendMessage(ChatColor.GOLD + "------------------------");
			int xp = 0;
			xp += (this.playerKillsMap.get(gp) * PluginConfig.getXpPerKill(gp.getBukkitPlayer()));
			if (getPlayerTeam(gp) == team)
				xp += PluginConfig.getXpPerWin(gp.getBukkitPlayer());
			IslandPlayer ip = PlayerManager.get(gp.getBukkitPlayer());
			ip.setXP(ip.getXP() + xp);
		}
		for (Villager v : new ArrayList<Villager>(this.villagers.keySet())) {
			this.villagers.remove(v);
			v.setHealth(0);
		}
		GameController.get().remove(this);
		GameController.get().sendUpdate();

		Bukkit.getScheduler().runTaskLater(EggWars.get(), new Runnable() {
			public void run() {
				for (IGamePlayer igp : getPlayers()) {
					GamePlayer gp = (GamePlayer) igp;
					onPlayerLeave(gp, true, false, true);
				}
			}
		}, 30L);

	}

	public void sendMessage(ChatColor cc, String msgCode, String[] components) {
		for (IGamePlayer gamePlayer : getPlayers()) {
			gamePlayer.getBukkitPlayer()
					.sendMessage(cc + LanguageMain.get(gamePlayer.getBukkitPlayer(), msgCode, components));
		}
	}

	public List<GamePlayer> getTeamPlayers(ITeam team) {
		List<GamePlayer> a = Lists.newArrayList();
		for (GamePlayer gp : this.playerTeamMap.keySet()) {
			if (this.playerTeamMap.get(gp) == team) {
				a.add(gp);
			}
		}
		return a;
	}

	public List<IGamePlayer> getTeamIPlayers(ITeam team) {
		List<IGamePlayer> a = Lists.newArrayList();
		for (GamePlayer gp : this.playerTeamMap.keySet()) {
			if (this.playerTeamMap.get(gp) == team) {
				a.add(gp);
			}
		}
		return a;
	}

	@SuppressWarnings("incomplete-switch")
	public void onTick() {
		if (!isBuilt()) {
			BlockPlacerPlayer entry = this.bpp;
			int blocks = 0;
			int maxBlocks = 0;
			double percentage = 100.0D;
			if (entry != null) {
				blocks = entry.getQueue().size();
				maxBlocks = entry.getMaxQueueBlocks();
			}
			int newMax = Math.max(blocks, maxBlocks);
			if (newMax > 0) {
				percentage = 100 - 100 * blocks / newMax;
			}

			for (GamePlayer gp : toJoin) {
				BossBarAPI.setMessage(gp.getBukkitPlayer(), LanguageMain.get(gp.getBukkitPlayer(), "game.building.bar",
						new String[] { Math.round(percentage) + "%" }));
				BossBarAPI.setHealth(gp.getBukkitPlayer(), (float) percentage);
			}
		}

		if (this.timer <= 0 && this.gameState == IGameState.WAITING || this.gameState == IGameState.PLAYING) {
			return;
		}

		this.timer -= 1;

		for (Villager v : this.villagers.keySet()) {
			v.teleport(this.villagers.get(v));
		}

		switch (this.gameState) {
		case WAITING:
			if (this.timer == 0) {
				if (hasReachedMinimumPlayers()) {
					onGameStart();
				} else {
					sendMessage("sw.nominplayers", new String[] { getMinimumPlayers() + "" });
					this.timer = 60;
				}
			} else if ((this.timer % 10 == 0 || this.timer <= 5) && this.timer != 1) {
				for (IGamePlayer gp : getPlayers()) {
					gp.getBukkitPlayer()
							.sendMessage(LanguageMain.get(gp.getBukkitPlayer(), "sw.starting",
									new String[] { Integer.toString(this.timer),
											LanguageMain.get(gp.getBukkitPlayer(), "sw.seconds") }));
				}
			} else if (this.timer == 1) {
				for (IGamePlayer gp : getPlayers()) {
					gp.getBukkitPlayer()
							.sendMessage(LanguageMain.get(gp.getBukkitPlayer(), "sw.starting",
									new String[] { Integer.toString(this.timer),
											LanguageMain.get(gp.getBukkitPlayer(), "sw.second") }));
				}
			}
			break;
		}
	}

	public IGameState getState() {
		return this.gameState;
	}

	public boolean isFull() {
		return getPlayerCount() == this.slots;
	}

	public int getMinimumPlayers() {
		return 4;
	}

	public boolean hasReachedMinimumPlayers() {
		return getPlayerCount() >= getMinimumPlayers();
	}

	public void sendMessage(String message) {
		for (IGamePlayer gamePlayer : getPlayers()) {
			gamePlayer.getBukkitPlayer().sendMessage(message);
		}
	}

	public void sendMessage(String msgCode, String[] components) {
		for (IGamePlayer gamePlayer : getPlayers()) {
			gamePlayer.getBukkitPlayer()
					.sendMessage(LanguageMain.get(gamePlayer.getBukkitPlayer(), msgCode, components));
		}
	}

	public int getPlayerCount() {
		return this.playerCount;
	}

	public java.util.Collection<IGamePlayer> getPlayers() {
		List<IGamePlayer> playerList = com.google.common.collect.Lists.newArrayList();

		for (GamePlayer gamePlayer : this.playerTeamMap.keySet()) {
			if (gamePlayer != null) {
				playerList.add(gamePlayer);
			}
		}

		return playerList;
	}

	public void addSpawns(Location[] spawns) {
		this.spawnPlaces = spawns;
	}

	private void registerScoreboard(GamePlayer gp, boolean first) {
		Scoreboard scoreboard = null;
		if (first) {
			scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		} else {
			scoreboard = gp.getBukkitPlayer().getScoreboard();
			if (scoreboard.getObjective("info") != null)
				scoreboard.getObjective("info").unregister();
		}
		Objective objective = scoreboard.registerNewObjective("info", "dummy");
		objective.setDisplaySlot(org.bukkit.scoreboard.DisplaySlot.SIDEBAR);
		objective.setDisplayName("�c�lEggWars");
		if (gameState == IGameState.WAITING)
			objective.getScore(LanguageMain.get(gp, "ew.selectteam")).setScore(0);
		else {
			for (ITeam team : getAliveTeams())
				objective
						.getScore((hasEgg(team) ? ChatColor.GREEN + "\u2713" : ChatColor.RED + "\u2717") + " "
								+ team.getColor() + StringUtils.formatString(team.toString()))
						.setScore(getTeamIPlayers(team).size());
		}
		if (first)
			gp.getBukkitPlayer().setScoreboard(scoreboard);
	}

	public void registerScoreboardAll(boolean first) {
		for (IGamePlayer igp : getPlayers()) {
			GamePlayer gp = (GamePlayer) igp;
			registerScoreboard(gp, first);
		}
	}

	public void changeTeam(GamePlayer gp, ITeam newTeam) {
		if (PartyManager.getPlayerParty(gp.getBukkitPlayer()) != null) {
			Player p = gp.getBukkitPlayer();
			Party party = PartyManager.getPlayerParty(p);
			if (party.getOwner() == p) {
				for (Player p2 : party.getPlayers()) {
					if (PlayerController.get().get(p2).getGame() == this) {
						playerTeamMap.put(PlayerController.get().get(p2), newTeam);
						p2.sendMessage(newTeam.getColor() + LanguageMain.get(p2, "ctf.jointeam",
								new String[] { LanguageMain.get(p2, newTeam.getLanguageCode()) }));
					}
				}
			} else {
				p.sendMessage(ChatColor.RED + LanguageMain.get(gp, "ctf.onlypartyownerswitchteam"));
			}
		} else {
			playerTeamMap.put(gp, newTeam);
			gp.getBukkitPlayer().sendMessage(newTeam.getColor() + LanguageMain.get(gp, "ctf.jointeam",
					new String[] { LanguageMain.get(gp, newTeam.getLanguageCode()) }));
		}
	}

	private void setInvTeams(Player p) {
		p.getInventory().clear();
		p.getInventory().setItem(1, ItemsMain.createItem(Material.WOOL, 1, 11, ChatColor.BLUE
				+ LanguageMain.get(p, "pw.chooseteam", new String[] { LanguageMain.get(p, "team.blue") })));
		p.getInventory().setItem(7, ItemsMain.createItem(Material.WOOL, 1, 14, ChatColor.RED
				+ LanguageMain.get(p, "pw.chooseteam", new String[] { LanguageMain.get(p, "team.red") })));
		p.getInventory().setItem(3, ItemsMain.createItem(Material.WOOL, 1, 4, ChatColor.YELLOW
				+ LanguageMain.get(p, "pw.chooseteam", new String[] { LanguageMain.get(p, "team.yellow") })));
		p.getInventory().setItem(5, ItemsMain.createItem(Material.WOOL, 1, 5, ChatColor.GREEN
				+ LanguageMain.get(p, "pw.chooseteam", new String[] { LanguageMain.get(p, "team.green") })));
		PlayerUtil.refreshPlayer(p);
	}

	private void equilibrateTeams() {
		ArrayList<ArrayList<GamePlayer>> teams = new ArrayList<ArrayList<GamePlayer>>();
		ArrayList<GamePlayer> blue = new ArrayList<GamePlayer>();
		teams.add(blue);
		ArrayList<GamePlayer> red = new ArrayList<GamePlayer>();
		teams.add(red);
		ArrayList<GamePlayer> yellow = new ArrayList<GamePlayer>();
		teams.add(yellow);
		ArrayList<GamePlayer> green = new ArrayList<GamePlayer>();
		teams.add(green);
		Collections.shuffle(teams, new Random(System.nanoTime()));
		ArrayList<GamePlayer> neutral = new ArrayList<GamePlayer>();
		for (GamePlayer gp : this.playerTeamMap.keySet()) {
			if (this.playerTeamMap.get(gp) == ITeam.BLUE)
				blue.add(gp);
			else if (this.playerTeamMap.get(gp) == ITeam.RED)
				red.add(gp);
			else if (this.playerTeamMap.get(gp) == ITeam.YELLOW)
				yellow.add(gp);
			else if (this.playerTeamMap.get(gp) == ITeam.GREEN)
				green.add(gp);
			else
				neutral.add(gp);
		}
		if (blue.size() == red.size() && blue.size() == yellow.size() && blue.size() == green.size()
				&& neutral.size() < 1) {
			return;
		}
		int minSize = Integer.MAX_VALUE;
		ArrayList<GamePlayer> minTeam = null;
		int maxSize = Integer.MIN_VALUE;
		ArrayList<GamePlayer> maxTeam = null;
		for (ArrayList<GamePlayer> a : teams) {
			if (a.size() < minSize) {
				minSize = a.size();
				minTeam = a;
			}
		}
		boolean ret = true;
		if ((this.playerTeamMap.size() % 4) != 0 && neutral.size() < 1) {
			for (ArrayList<GamePlayer> a : teams) {
				if (a.size() == minSize)
					continue;
				if (a.size() == minSize + 1)
					continue;
				ret = false;
			}
			if (ret)
				return;
		}
		Random r = new Random(System.nanoTime());
		if (neutral.size() > 0) {
			while (neutral.size() > 0) {
				minSize = Integer.MAX_VALUE;
				for (ArrayList<GamePlayer> a : teams) {
					if (a.size() < minSize) {
						minSize = a.size();
						minTeam = a;
					}
				}
				minTeam.add(neutral.remove(r.nextInt(neutral.size())));
			}
		}
		while (true) {
			minSize = Integer.MAX_VALUE;
			maxSize = Integer.MIN_VALUE;
			for (ArrayList<GamePlayer> a : teams) {
				if (a.size() < minSize) {
					minSize = a.size();
					minTeam = a;
				}
				if (a.size() > maxSize) {
					maxSize = a.size();
					maxTeam = a;
				}
			}
			ret = true;
			for (ArrayList<GamePlayer> a : teams) {
				if (a.size() == minSize) {
					continue;
				}
				if (a.size() == minSize + 1) {
					continue;
				}
				ret = false;
			}
			if (ret) {
				transferToGame(blue, red, yellow, green);
				return;
			}
			minTeam.add(maxTeam.remove(r.nextInt(maxTeam.size())));
		}

	}

	public void transferToGame(ArrayList<GamePlayer> blue, ArrayList<GamePlayer> red, ArrayList<GamePlayer> yellow,
			ArrayList<GamePlayer> green) {
		this.playerTeamMap.clear();
		for (GamePlayer gp : blue) {
			this.playerTeamMap.put(gp, ITeam.BLUE);
		}
		for (GamePlayer gp : red) {
			this.playerTeamMap.put(gp, ITeam.RED);
		}
		for (GamePlayer gp : yellow) {
			this.playerTeamMap.put(gp, ITeam.YELLOW);
		}
		for (GamePlayer gp : green) {
			this.playerTeamMap.put(gp, ITeam.GREEN);
		}

	}

	public ITeam getPlayerTeam(IGamePlayer gp) {
		return this.playerTeamMap.get(gp);
	}

	public Location[] getSpawnPlaces() {
		return this.spawnPlaces;
	}

	public void setDefaultItems(GamePlayer gp, ITeam team) {
		PlayerUtil.clearInventory(gp.getBukkitPlayer());
	}

	public List<Player> getSpawningPlayers() {
		return spawning;
	}

	@Override
	public void setWEUtil(IWEUtils iweUtils) {
		this.weutil = iweUtils;
	}

	public boolean hasEgg(ITeam team) {
		return this.eggMap.get(team);
	}

	public void setEggBroken(ITeam team, boolean broken) {
		this.eggMap.put(team, !broken);
	}

	public void brokeEgg(ITeam team, Location l, GamePlayer gp) {
		this.eggMap.put(team, false);
		l.getBlock().setType(Material.AIR);
		sendMessage("ew.eggdestroyed", new String[] { team.getColor() + StringUtils.formatString(team.toString()),
				getPlayerTeam(gp).getColor() + gp.getName() });
		for (IGamePlayer igp : getTeamIPlayers(team)) {
			String title = "";
			String subtitle = "";
			try {
				String[] a = LanguageMain.get(igp, "ew.eggdestroyedtitle").split("\\n");
				title = a[0];
				subtitle = a[1];
			} catch (ArrayIndexOutOfBoundsException e) {
			}
			new Title(igp.getBukkitPlayer(), title, subtitle, 1, 5, 1);
		}
		registerScoreboardAll(false);
	}

	public void setEggBlock(ITeam team, Location l) {
		this.eggMap.put(team, true);
		byte colorID = 0;
		switch (team) {
		case RED:
			colorID = 14;
			break;
		case BLUE:
			colorID = 11;
			break;
		case YELLOW:
			colorID = 4;
			break;
		case GREEN:
			colorID = 5;
			break;
		default:
			break;
		}
		l.getBlock().setType(Material.STAINED_CLAY);
		l.getBlock().setData(colorID);
		l.add(0, 1, 0);
		l.getBlock().setType(Material.DRAGON_EGG);
	}

	public List<IGamePlayer> getSpectators() {
		List<IGamePlayer> a = Lists.newArrayList();
		for (GamePlayer gp : spectators)
			a.add(gp);
		return a;
	}

	public void addVillager(Location l) {
		this.toSpawnVillagers.add(l.clone());
	}

	private void setupVillagers() {
		for (Location l : toSpawnVillagers) {
			Villager v = (Villager) l.getWorld().spawnEntity(l.add(0, -0.5, 0), EntityType.VILLAGER);
			v.setCustomName(ChatColor.YELLOW + "EggWars");
			v.setCustomNameVisible(true);
			v.setAdult();
			v.setProfession(Profession.BLACKSMITH);
			EntityUtils.noAI(v);
			this.villagers.put(v, l);
		}
	}

	public boolean isVillager(Villager v) {
		return this.villagers.containsKey(v);
	}

	private void addGenerator(Generator g) {
		this.generators.add(g);
		GeneratorUtils.updateGeneratorSign(g);
	}

	public void addGenerator(Location l) {
		this.toMakeGenerators.add(l);
	}

	private void setupGenerators() {
		for (Location l : toMakeGenerators) {
			Block block = l.clone().add(0, 1, 0).getBlock();
			Sign s = (Sign) block.getState();
			int level = Integer.parseInt(s.getLine(1));
			Material b = l.clone().getBlock().getType();
			switch (b) {
			case IRON_BLOCK:
				addGenerator(new IronGenerator(level, l.clone()));
				break;
			case GOLD_BLOCK:
				addGenerator(new GoldGenerator(level, l.clone()));
				break;
			case DIAMOND_BLOCK:
				addGenerator(new DiamondGenerator(level, l.clone()));
				break;
			default:
			}
		}
	}

	public List<Generator> getGeneratorsAtTime(double time) {
		List<Generator> a = Lists.newArrayList();
		for (Generator g : this.generators)
			if (g.getCurrentLevel() != 0)
				if (g.getCurrentGLevel().getTime() == time)
					a.add(g);

		return a;
	}

	public List<Generator> getGenerators() {
		return generators;
	}

	public void addPlacedBlock(Block b) {
		if (!this.placedBlocks.contains(b))
			this.placedBlocks.add(b);
	}

	public void removePlacedBlock(Block b) {
		if (this.placedBlocks.contains(b))
			this.placedBlocks.remove(b);
	}

	public boolean isPlacedBlock(Block b) {
		return this.placedBlocks.contains(b);
	}

	public List<ITeam> getAliveTeams() {
		List<ITeam> teams = Lists.newArrayList();
		for (GamePlayer gp : this.playerTeamMap.keySet())
			if (!teams.contains(this.playerTeamMap.get(gp)))
				teams.add(this.playerTeamMap.get(gp));
		return teams;
	}

}