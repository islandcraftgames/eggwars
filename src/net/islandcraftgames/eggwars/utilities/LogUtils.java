 package net.islandcraftgames.eggwars.utilities;
 
 import java.util.logging.Level;

import net.islandcraftgames.eggwars.EggWars;
 
 public class LogUtils
 {
   public static void log(Level level, String message, Object... args)
   {
     if (args.length > 0) {
       message = String.format(message, args);
     }
     
     EggWars.get().getLogger().log(level, message);
   }
   
   public static void log(Level level, Class<?> clazz, String message, Object... args) {
     log(level, String.format("[%s] %s", new Object[] { clazz.getSimpleName(), message }), args);
   }
 }