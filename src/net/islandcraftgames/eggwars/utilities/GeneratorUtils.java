package net.islandcraftgames.eggwars.utilities;

import org.bukkit.block.Sign;

import me.islandcraft.mainhub.utils.StringUtils;
import net.islandcraftgames.eggwars.generators.Generator;

public class GeneratorUtils {
	
	public static void updateGeneratorSign(Generator g){
		Sign s = (Sign) g.getSignLocation(false).getBlock().getState();
		s.setLine(0, StringUtils.formatString(g.getType().toString()));
		s.setLine(1, "Generator");
		s.setLine(2, "Level " + g.getCurrentLevel());
		s.update();
	}

}
