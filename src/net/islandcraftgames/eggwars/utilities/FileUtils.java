 package net.islandcraftgames.eggwars.utilities;
 
 import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import org.bukkit.plugin.Plugin;
 
 public class FileUtils
 {
   public static boolean deleteFolder(@javax.annotation.Nonnull File file)
   {
     if (file.exists()) {
       boolean result = true;
       
       if (file.isDirectory()) {
         File[] contents = file.listFiles();
         
         if (contents != null) {
           for (File f : contents) {
             result = (result) && (deleteFolder(f));
           }
         }
       }
       
       return (result) && (file.delete());
     }
     
     return false;
   }
   
   public static void saveResource(Plugin plugin, String resourcePath, File outFile, boolean replace) {
     if ((resourcePath == null) || (resourcePath.equals(""))) {
       throw new IllegalArgumentException("ResourcePath cannot be null or empty");
     }
     
     resourcePath = resourcePath.replace('\\', '/');
     InputStream in = plugin.getResource(resourcePath);
     if (in == null) {
       throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found.");
     }
     
     int lastIndex = resourcePath.lastIndexOf('/');
     File outDir = new File(plugin.getDataFolder(), resourcePath.substring(0, lastIndex >= 0 ? lastIndex : 0));
     
     if ((!outDir.exists()) && (!outDir.mkdirs())) {
       return;
     }
     try
     {
       if ((!outFile.exists()) || (replace)) {
         OutputStream out = new java.io.FileOutputStream(outFile);
         byte[] buf = new byte['?'];
         int len;
         while ((len = in.read(buf)) > 0) {
           out.write(buf, 0, len);
         }
         out.close();
         in.close();
       } else {
         plugin.getLogger().log(java.util.logging.Level.WARNING, "Could not save " + outFile.getName() + " to " + outFile + " because " + outFile.getName() + " already exists.");
       }
     }
     catch (java.io.IOException ex) {
       plugin.getLogger().log(java.util.logging.Level.SEVERE, "Could not save " + outFile.getName() + " to " + outFile, ex);
     }
   }
 }