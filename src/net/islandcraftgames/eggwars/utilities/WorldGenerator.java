 package net.islandcraftgames.eggwars.utilities;
 
 import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
 
 public class WorldGenerator extends org.bukkit.generator.ChunkGenerator
 {
   public byte[] generate(World world, Random random, int cx, int cz)
   {
     return new byte[65536];
   }
   
   public Location getFixedSpawnLocation(World world, Random random)
   {
     if (!world.isChunkLoaded(0, 0)) {
       world.loadChunk(0, 0);
     }
     return new Location(world, 0.0D, 64.0D, 0.0D);
   }
 }