package net.islandcraftgames.eggwars.generators.types;

import org.bukkit.Location;
import org.bukkit.Material;

import net.islandcraftgames.eggwars.generators.Generator;
import net.islandcraftgames.eggwars.generators.GeneratorType;

public class DiamondGenerator extends Generator{
	
	public DiamondGenerator(int level){
		this(level, null);
	}
	
	public DiamondGenerator(int level, Location l){
		material = Material.DIAMOND;
		type = GeneratorType.DIAMOND;
		nameCode = "eggwars.diamondtoken";
		currentLevel = level;
		location = l;
		addLevel(1, 10, 5, Material.DIAMOND);
		addLevel(2, 5, 15, Material.DIAMOND);
		addLevel(3, 2.5, 25, Material.DIAMOND);
		addLevel(4, 1, 32, Material.DIAMOND);
	}

}
