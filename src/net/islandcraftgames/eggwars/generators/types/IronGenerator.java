package net.islandcraftgames.eggwars.generators.types;

import org.bukkit.Location;
import org.bukkit.Material;

import net.islandcraftgames.eggwars.generators.Generator;
import net.islandcraftgames.eggwars.generators.GeneratorType;

public class IronGenerator extends Generator{
	
	public IronGenerator(int level){
		this(level, null);
	}
	
	public IronGenerator(int level, Location l){
		material = Material.IRON_INGOT;
		type = GeneratorType.IRON;
		nameCode = "eggwars.irontoken";
		currentLevel = level;
		location = l;
		addLevel(1, 1.5, 5, Material.IRON_INGOT);
		addLevel(2, 1, 15, Material.IRON_INGOT);
		addLevel(3, 0.75, 10, Material.GOLD_INGOT);
		addLevel(4, 0.5, 20, Material.GOLD_INGOT);
		addLevel(5, 0.25, 5, Material.DIAMOND);
	}

}
