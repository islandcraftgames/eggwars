package net.islandcraftgames.eggwars.generators.types;

import org.bukkit.Location;
import org.bukkit.Material;

import net.islandcraftgames.eggwars.generators.Generator;
import net.islandcraftgames.eggwars.generators.GeneratorType;

public class GoldGenerator extends Generator{
	
	public GoldGenerator(int level){
		this(level, null);
	}
	
	public GoldGenerator(int level, Location l){
		material = Material.GOLD_INGOT;
		type = GeneratorType.GOLD;
		nameCode = "eggwars.goldtoken";
		currentLevel = level;
		location = l;
		addLevel(1, 5, 10, Material.GOLD_INGOT);
		addLevel(2, 2.5, 20, Material.GOLD_INGOT);
		addLevel(3, 1.5, 10, Material.DIAMOND);
		addLevel(4, 0.75, 25, Material.DIAMOND);
	}

}
