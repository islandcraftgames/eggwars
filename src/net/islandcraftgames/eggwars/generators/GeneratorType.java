package net.islandcraftgames.eggwars.generators;

public enum GeneratorType {
	IRON, GOLD, DIAMOND;

	public String getName() {
		return toString().toUpperCase().substring(0, 1) + toString().toLowerCase().substring(1);
	}
}
