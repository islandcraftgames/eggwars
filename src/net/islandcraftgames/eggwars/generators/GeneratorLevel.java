package net.islandcraftgames.eggwars.generators;

import org.bukkit.Material;

public class GeneratorLevel {

	private int level;
	private double time;
	private int cost;
	private Material costItem;

	public GeneratorLevel(int level, double time, int cost, Material costItem) {
		this.level = level;
		this.time = time;
		this.cost = cost;
		this.costItem = costItem;
	}

	public int getCost() {
		return cost;
	}

	public Material getCostItem() {
		return costItem;
	}

	public int getLevel() {
		return level;
	}

	public double getTime() {
		return time;
	}

}
