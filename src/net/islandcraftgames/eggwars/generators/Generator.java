package net.islandcraftgames.eggwars.generators;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import me.islandcraft.mainhub.utils.ItemUtils;
import net.islandcraftgames.eggwars.utilities.GeneratorUtils;

public abstract class Generator {

	protected Material material;
	protected GeneratorType type;
	protected String nameCode;
	protected int currentLevel;
	protected Location location;

	private List<GeneratorLevel> levels = Lists.newArrayList();

	protected void addLevel(int level, double time, int cost, Material costMaterial) {
		levels.add(new GeneratorLevel(level, time, cost, costMaterial));
	}

	public Material getMaterial() {
		return material;
	}

	public GeneratorType getType() {
		return type;
	}

	public String getNameCode() {
		return nameCode;
	}

	public int getCurrentLevel() {
		return currentLevel;
	}

	public GeneratorLevel getCurrentGLevel() {
		return getLevel(currentLevel);
	}

	public GeneratorLevel getLevel(int level) {
		for (GeneratorLevel l : levels)
			if (l.getLevel() == level)
				return l;
		return null;
	}

	public List<GeneratorLevel> getAllLevels() {
		return levels;
	}

	public Location getLocation(boolean middle) {
		if (middle)
			return location.clone().add(0.5, 0.5, 0.5);
		return location.clone();
	}

	public Location getSignLocation(boolean middle) {
		if (middle)
			return location.clone().add(0.5, 1.5, 0.5);
		return location.clone().add(0, 1, 0);
	}

	public World getWorld() {
		return this.location.getWorld();
	}

	public void setCurrentLevel(int level) {
		this.currentLevel = level;
	}

	public boolean hasNextLevel() {
		return getLevel(currentLevel + 1) != null;
	}

	public GeneratorLevel getNextGLevel() {
		return getLevel(currentLevel + 1);
	}

	public void upgrade(Player p) {
		if (!ItemUtils.removeFromInventory(p,
				new ItemStack(getNextGLevel().getCostItem(), getNextGLevel().getCost()))) {
			p.sendMessage(ChatColor.RED + "You don't have resources to upgrade this generator!");
			return;
		}
		currentLevel++;
		p.sendMessage(ChatColor.GREEN + "Generator successfuly upgraded!");
		GeneratorUtils.updateGeneratorSign(this);
	}

}
