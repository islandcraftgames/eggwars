package net.islandcraftgames.eggwars.tasks;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGameState;
import me.islandcraft.mainhub.items.ItemsMain;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.controllers.GameController;
import net.islandcraftgames.eggwars.game.Game;
import net.islandcraftgames.eggwars.generators.Generator;

public class GeneratorTask extends BukkitRunnable {

	private double time;

	public GeneratorTask(double d) {
		time = d;
		runTaskTimer(EggWars.get(), 0L, (long) (time * 20));
	}

	@Override
	public void run() {
		for (IGame g : GameController.get().getAll())
			if (g.getState() == IGameState.PLAYING)
				for (Generator gen : ((Game) g).getGeneratorsAtTime(this.time))
					if (getDroppedItemsAt(gen.getSignLocation(true)) < 32)
						gen.getWorld()
								.dropItem(gen.getSignLocation(false).getBlock().getLocation().add(+0.5, +0.2, +0.5),
										ItemsMain.createItem(gen.getMaterial(), 1,
												ChatColor.GOLD + gen.getType().getName() + " Token"))
								.setVelocity(new Vector(0, 0, 0));
	}

	public static int getDroppedItemsAt(Location l) {
		int i = 0;
		for (Entity e : l.getWorld().getNearbyEntities(l, 1, 1, 1))
			if (e.getType() == EntityType.DROPPED_ITEM)
				i += ((Item) e).getItemStack().getAmount();
		return i;
	}

}
