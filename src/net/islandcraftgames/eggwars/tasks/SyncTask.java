package net.islandcraftgames.eggwars.tasks;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.config.PluginConfig;
import net.islandcraftgames.eggwars.controllers.GameController;
import net.islandcraftgames.eggwars.storage.SignStorage;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutUpdateSign;

public class SyncTask implements Runnable {
	@SuppressWarnings("unused")
	private int tickCounter;

	public void run() {
		for (IGame game : GameController.get().getAll()) {
			game.onTick();
		}

		for (Sign s : SignStorage.getAllSigns()) {
			if (ChatColor.stripColor(s.getLine(0)).equalsIgnoreCase("[" + EggWars.getGameName() + "]")) {
				for (Player p : s.getWorld().getPlayers()) {
					if (p.getLocation().distanceSquared(s.getLocation()) < 100) {
						IChatBaseComponent[] lines = new IChatBaseComponent[] { toJSON(s.getLine(0)),
								toJSON(s.getLine(1)),
								toJSON(GameController.get().getPlayersWaiting(s.getLine(1)) + " "
										+ LanguageMain.get(p, "sw.waiting")),
								toJSON(GameController.get().getPlayersPlaying(s.getLine(1)) + " "
										+ LanguageMain.get(p, "sw.playing")) };
						PacketPlayOutUpdateSign packet = new PacketPlayOutUpdateSign(
								((CraftWorld) s.getWorld()).getHandle(),
								new BlockPosition(s.getX(), s.getY(), s.getZ()), lines);
						((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
					}
				}
			} else {
				SignStorage.removeSign(s.getLocation());
			}
		}

		if (!PluginConfig.isKitsOn())
			return;

		File directory = EggWars.get().getDataFolder();
		File villagersFile = new File(directory, "villagers.yml");

		if (!villagersFile.exists()) {
			return;
		}

		YamlConfiguration villagers = new YamlConfiguration();

		try {
			villagers.load(villagersFile);
			for (Entity e : getWorld().getEntities()) {
				if (e.getUniqueId().toString().equalsIgnoreCase(villagers.getString("UUID"))) {
					e.teleport(getLoc());
					break;
				}
			}
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	private Location getLoc() {

		File directory = EggWars.get().getDataFolder();
		File villagersFile = new File(directory, "villagers.yml");

		YamlConfiguration villagers = new YamlConfiguration();

		try {
			villagers.load(villagersFile);
			String a = villagers.getString("location");
			String[] b = a.split(", ");
			World w = Bukkit.getWorld(b[0]);
			double x = Double.parseDouble(b[1]);
			double y = Double.parseDouble(b[2]);
			double z = Double.parseDouble(b[3]);
			float yaw = Float.parseFloat(b[4]);
			float pitch = Float.parseFloat(b[5]);
			return new Location(w, x, y, z, yaw, pitch);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
			return new Location(Bukkit.getWorld("world"), 0, 64, 0, 0, 0);
		}

	}

	private World getWorld() {

		File directory = EggWars.get().getDataFolder();
		File villagersFile = new File(directory, "villagers.yml");

		YamlConfiguration villagers = new YamlConfiguration();

		try {
			villagers.load(villagersFile);
			String a = villagers.getString("location");
			String[] b = a.split(", ");
			return Bukkit.getWorld(b[0]);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
			return Bukkit.getWorld("world");
		}
	}

	private IChatBaseComponent toJSON(String text) {
		return ChatSerializer.a("{\"text\": \"" + text + "\"}");
	}
}