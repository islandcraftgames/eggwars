package net.islandcraftgames.eggwars.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.islandcraft.gameapi.IGameState;
import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.config.PluginConfig;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.player.GamePlayer;

@CommandDescription("Starts a SkyWars game")
@CommandPermissions({ "skywars.command.start" })
public class StartCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		GamePlayer gamePlayer = PlayerController.get().get(
				(org.bukkit.entity.Player) sender);

		if (!gamePlayer.isPlaying()) {
			sender.sendMessage(ChatColor.RED
					+ LanguageMain.get(gamePlayer, "sw.notplaying"));
		} else if (gamePlayer.getGame().getState() != IGameState.WAITING) {
			sender.sendMessage(ChatColor.RED
					+ LanguageMain.get(gamePlayer, "sw.alreadystarted"));
		} else if (gamePlayer.getGame().getPlayerCount() < 2) {
			sender.sendMessage(ChatColor.RED
					+ LanguageMain.get(gamePlayer, "sw.insuficientplayers"));
		} else if ((PluginConfig.buildSchematic())
				&& (!gamePlayer.getGame().isBuilt())) {
			sender.sendMessage(ChatColor.RED
					+ LanguageMain.get(gamePlayer, "sw.creatingarena"));
		} else {
			gamePlayer.getGame().onGameStart();
		}

		return true;
	}
}