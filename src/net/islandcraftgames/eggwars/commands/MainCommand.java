package net.islandcraftgames.eggwars.commands;

import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import me.islandcraft.mainhub.language.LanguageMain;
import net.islandcraftgames.eggwars.config.PluginConfig;
import net.islandcraftgames.eggwars.tasks.GeneratorTask;

public class MainCommand implements CommandExecutor {
	private Map<String, CommandExecutor> subCommandMap = Maps.newHashMap();

	public MainCommand() {
		this.subCommandMap.put("start", new StartCommand());
		if (PluginConfig.isKitsOn())
			this.subCommandMap.put("villager", new VillagerCommand());
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can use that command!");
			return true;
		}

		Player player = (Player) sender;
		if (!player.hasPermission("staff"))
			return false;
		if (args.length == 0) {
			printHelp(player, label);
			player.sendMessage(GeneratorTask.getDroppedItemsAt(player.getLocation()) + "");
			return true;
		}

		String subCommandName = args[0].toLowerCase();
		if (!this.subCommandMap.containsKey(subCommandName)) {
			printHelp(player, label);
			return true;
		}

		CommandExecutor subCommand = (CommandExecutor) this.subCommandMap.get(subCommandName);
		if (!hasPermission(player, subCommand)) {
			player.sendMessage(ChatColor.RED + LanguageMain.get(player, "sw.noperm"));
			return true;
		}

		return subCommand.onCommand(sender, command, label, args);
	}

	private boolean hasPermission(Player bukkitPlayer, CommandExecutor cmd) {
		CommandPermissions permissions = (CommandPermissions) cmd.getClass().getAnnotation(CommandPermissions.class);
		if (permissions == null) {
			return true;
		}

		for (String permission : permissions.value()) {
			if (bukkitPlayer.hasPermission(permission)) {
				return true;
			}
		}

		return false;
	}

	private void printHelp(Player bukkitPlayer, String label) {
		if (!bukkitPlayer.isOp()) {
			bukkitPlayer.sendMessage(ChatColor.RED + LanguageMain.get(bukkitPlayer, "sw.noperm"));
		}
		bukkitPlayer.sendMessage(ChatColor.RED + LanguageMain.get(bukkitPlayer, "sw.commandinfo"));

		for (Map.Entry<String, CommandExecutor> commandEntry : this.subCommandMap.entrySet()) {
			if (hasPermission(bukkitPlayer, (CommandExecutor) commandEntry.getValue())) {
				String description = LanguageMain.get(bukkitPlayer, "sw.nodescription");

				CommandDescription cmdDescription = (CommandDescription) ((CommandExecutor) commandEntry.getValue())
						.getClass().getAnnotation(CommandDescription.class);
				if (cmdDescription != null) {
					description = cmdDescription.value();
				}

				bukkitPlayer
						.sendMessage("�7/" + label + " " + (String) commandEntry.getKey() + " �f-�e " + description);
			}
		}
	}
}