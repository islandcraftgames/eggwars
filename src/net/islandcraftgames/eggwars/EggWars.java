package net.islandcraftgames.eggwars;

import java.io.File;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.World;

import com.google.common.collect.Lists;
import com.onarandombox.MultiverseCore.MultiverseCore;

import me.islandcraft.gameapi.IGameType;
import me.islandcraft.gameapi.controllers.ClassController;
import me.islandcraft.mainhub.Main;
import me.islandcraft.mainhub.economy.EcoMain;
import net.islandcraftgames.eggwars.controllers.GameController;
import net.islandcraftgames.eggwars.controllers.PlayerController;
import net.islandcraftgames.eggwars.generators.GeneratorLevel;
import net.islandcraftgames.eggwars.generators.types.DiamondGenerator;
import net.islandcraftgames.eggwars.generators.types.GoldGenerator;
import net.islandcraftgames.eggwars.generators.types.IronGenerator;
import net.islandcraftgames.eggwars.tasks.GeneratorTask;
import net.islandcraftgames.eggwars.utilities.FileUtils;
import net.islandcraftgames.eggwars.utilities.WorldGenerator;

public class EggWars extends org.bukkit.plugin.java.JavaPlugin {
	private static EggWars instance;
	private static final IGameType gameType = IGameType.EGGWARS;
	private static final String gameName = "EggWars";
	private static final String gameAlias = "ew";
	private static final int shopRows = 1;

	public void onEnable() {
		instance = this;

		deleteIslandWorlds();

		net.islandcraftgames.eggwars.controllers.SchematicController.get();
		net.islandcraftgames.eggwars.controllers.WorldController.get();
		ClassController.setGameController(gameType, GameController.get());
		ClassController.setPlayerController(gameType, PlayerController.get());
		net.islandcraftgames.eggwars.controllers.ChestController.get();

		getCommand(gameName).setExecutor(new net.islandcraftgames.eggwars.commands.MainCommand());

		Bukkit.getPluginManager().registerEvents(new net.islandcraftgames.eggwars.listeners.PlayerListener(), this);
		Bukkit.getPluginManager().registerEvents(new net.islandcraftgames.eggwars.listeners.EntityListener(), this);
		Bukkit.getPluginManager().registerEvents(new net.islandcraftgames.eggwars.listeners.BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new net.islandcraftgames.eggwars.listeners.VillagerListener(), this);

		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new net.islandcraftgames.eggwars.tasks.SyncTask(), 20L,
				20L);
		GameController.get().mantainArenas();
		setupGeneratorTasks();
	}

	public void onDisable() {
		GameController.get().shutdown();
		PlayerController.get().shutdown();
		deleteIslandWorlds();
	}

	private void setupGeneratorTasks() {
		List<Double> a = Lists.newArrayList();
		for (GeneratorLevel level : new IronGenerator(0).getAllLevels()) {
			if (a.contains(level.getTime()))
				continue;
			a.add(level.getTime());
			new GeneratorTask(level.getTime());
		}
		for (GeneratorLevel level : new GoldGenerator(0).getAllLevels()) {
			if (a.contains(level.getTime()))
				continue;
			a.add(level.getTime());
			new GeneratorTask(level.getTime());
		}
		for (GeneratorLevel level : new DiamondGenerator(0).getAllLevels()) {
			if (a.contains(level.getTime()))
				continue;
			a.add(level.getTime());
			new GeneratorTask(level.getTime());
		}
	}

	private void deleteIslandWorlds() {
		File workingDirectory = new File(".");
		File[] contents = workingDirectory.listFiles();

		if (contents != null) {
			for (File file : contents) {
				if ((file.isDirectory()) && (file.getName().matches(gameAlias + "-\\d+"))) {

					World world = getServer().getWorld(file.getName());
					Boolean result = Boolean.valueOf(false);
					if (Bukkit.getPluginManager().getPlugin("Multiverse-Core") != null) {
						MultiverseCore multiVerse = (MultiverseCore) Bukkit.getPluginManager()
								.getPlugin("Multiverse-Core");
						if (world != null) {
							try {
								result = Boolean.valueOf(multiVerse.getMVWorldManager().deleteWorld(world.getName()));
							} catch (IllegalArgumentException ignored) {
								result = Boolean.valueOf(false);
							}
						} else {
							result = Boolean
									.valueOf(multiVerse.getMVWorldManager().removeWorldFromConfig(file.getName()));
						}
					}
					if (!result.booleanValue()) {
						if (world != null) {
							result = Boolean.valueOf(getServer().unloadWorld(world, true));
							if (result.booleanValue() == true) {
								getLogger().log(Level.INFO, "World ''{0}'' was unloaded from memory.", file.getName());
							} else {
								getLogger().log(Level.SEVERE, "World ''{0}'' could not be unloaded.", file.getName());
							}
						}
						result = Boolean.valueOf(FileUtils.deleteFolder(file));
						if (result.booleanValue() == true) {
							getLogger().log(Level.INFO, "World ''{0}'' was deleted.", file.getName());
						} else {
							getLogger().log(Level.SEVERE, "World ''{0}'' was NOT deleted.", file.getName());
							getLogger().log(Level.SEVERE, "Are you sure the folder {0} exists?", file.getName());
							getLogger().log(Level.SEVERE, "Please check your file permissions on ''{0}''",
									file.getName());
						}
					}
				}
			}
		}

	}

	public static EggWars get() {
		return instance;
	}

	public static EcoMain getEconomy() {
		return Main.getEconomy();
	}

	public WorldGenerator getDefaultWorldGenerator(String worldName, String id) {
		return new WorldGenerator();
	}

	public static IGameType getGameType() {
		return gameType;
	}

	public static String getGameName() {
		return gameName;
	}

	public static String getGameAlias() {
		return gameAlias;
	}

	public static int getShopSize() {
		return shopRows * 9;
	}

	public static String getPluginName() {
		return EggWars.get().getDescription().getName();
	}

}