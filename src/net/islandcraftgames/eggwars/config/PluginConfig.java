package net.islandcraftgames.eggwars.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.islandcraft.mainhub.Main;

public class PluginConfig {
	private static FileConfiguration storage;
	private static Location lobbySpawn;
	private static List<String> whitelistedCommands = new ArrayList<String>();

	static {
		storage = Main.get().getConfig();
		String lobby = "arcadelobby";

		String world = storage.getString(lobby).split(", ")[3];
		int x = Integer.parseInt(storage.getString(lobby).split(", ")[0]);
		int y = Integer.parseInt(storage.getString(lobby).split(", ")[1]);
		int z = Integer.parseInt(storage.getString(lobby).split(", ")[2]);

		lobbySpawn = new Location(Bukkit.getWorld(world), x, y, z);
	}

	public static Location getLobbySpawn() {
		return lobbySpawn;
	}

	public static void setLobbySpawn(Location location) {
		lobbySpawn = location.clone();
		storage.set("lobby.world", lobbySpawn.getWorld().getName());
		storage.set("lobby.spawn", String.format(
				Locale.US,
				"%.2f %.2f %.2f %.2f %.2f",
				new Object[] { Double.valueOf(location.getX()),
						Double.valueOf(location.getY()),
						Double.valueOf(location.getZ()),
						Float.valueOf(location.getYaw()),
						Float.valueOf(location.getPitch()) }));
		saveConfig();
	}

	public static boolean isCommandWhitelisted(String command) {
		return whitelistedCommands.contains(command.replace("/", ""));
	}

	public static int getIslandsPerWorld() {
		return 200;
	}

	public static int getIslandBuffer() {
		return 5;
	}

	public static int getScorePerKill(Player p) {
		double a = 50;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getXpPerKill(Player p) {
		double a = 1;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getScorePerWin(Player p) {
		double a = 200;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getXpPerWin(Player p) {
		double a = 4;
		if (p.hasPermission("vip.vip++")) {
			a = a * 3;
		} else if (p.hasPermission("vip.vip+")) {
			a = a * 2;
		} else if (p.hasPermission("vip.vip")) {
			a = a * 1.5;
		}

		return (int) Math.floor(a);
	}

	public static int getScorePerDeath(Player player) {
		return 0;
	}

	public static int getScorePerLeave(Player player) {
		return 0;
	}

	public static boolean buildSchematic() {
		return false;
	}

	public static int blocksPerTick() {
		return 20;
	}

	public static long buildInterval() {
		return 1L;
	}

	public static boolean buildCages() {
		return true;
	}

	public static boolean ignoreAir() {
		return false;
	}

	public static boolean fillEmptyChests() {
		return true;
	}

	public static boolean fillPopulatedChests() {
		return true;
	}

	public static boolean useEconomy() {
		return true;
	}

	public static boolean clearInventory() {
		return true;
	}

	public static boolean saveInventory() {
		return false;
	}

	public static void setSchematicConfig(String schematicFile, int teams) {
		String schematicPath = "schematics."
				+ schematicFile.replace(".schematic", "");
		if (!storage.isSet(schematicPath)) {
			storage.set(schematicPath + ".teams", Integer.valueOf(teams));
			storage.set(schematicPath + ".timer", Integer.valueOf(21));
			saveConfig();
		}
	}

	public static void migrateConfig() {
		if (storage.isSet("fill-chests")) {
			Boolean fill = Boolean.valueOf(storage.getBoolean("fill-chests"));
			storage.set("fill-empty-chests", fill);
			storage.set("fill-populated-chests", fill);
			storage.set("fill-chests", null);
		}
		if (!storage.isSet("lobby.radius")) {
			storage.set("lobby.radius", Integer.valueOf(0));
		}
		if (storage.isSet("island-size")) {
			storage.set("island-size", null);
		}
		if (!storage.isSet("island-buffer")) {
			storage.set("island-buffer", Integer.valueOf(5));
		}
		if (!storage.isSet("disable-kits")) {
			storage.set("disable-kits", Boolean.valueOf(false));
		}
		saveConfig();
	}

	private static boolean saveConfig() {
		File file = new File("./plugins/SkyWars/config.yml");
		try {
			storage.save(file);
			return true;
		} catch (IOException ignored) {
		}
		return false;
	}

	public static boolean getSpectator() {
		return false;
	}

	public static boolean isKitsOn() {
		return false;
	}
}