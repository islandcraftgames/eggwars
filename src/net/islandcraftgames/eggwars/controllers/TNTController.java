package net.islandcraftgames.eggwars.controllers;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.TNTPrimed;

import com.google.common.collect.Maps;

import net.islandcraftgames.eggwars.game.Game;

public class TNTController {

	private static HashMap<UUID, Game> tnts = Maps.newHashMap();

	public static void addTNT(Entity e, Game game) {
		if (e instanceof TNTPrimed)
			tnts.put(e.getUniqueId(), game);
	}
	
	public static Game getTNTGame(UUID uuid){
		return tnts.get(uuid);
	}
	
	public static void removeTNT(UUID uuid){
		tnts.remove(uuid);
	}

}
