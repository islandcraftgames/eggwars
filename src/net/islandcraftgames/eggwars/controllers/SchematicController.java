package net.islandcraftgames.eggwars.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Material;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;

import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.utilities.LogUtils;

@SuppressWarnings({ "deprecation" })
public class SchematicController {
	private static SchematicController instance;
	private final Random random = new Random();

	private final Map<String, CuboidClipboard> schematicMap = Maps.newHashMap();
	private final Map<CuboidClipboard, Vector[]> spawnCache = Maps.newHashMap();
	private final Map<CuboidClipboard, Vector[]> villagerCache = Maps.newHashMap();
	private final Map<CuboidClipboard, List<Vector>> generatorCache = Maps.newHashMap();
	private int schematicSize = 0;

	public SchematicController() {
		File dataDirectory = EggWars.get().getDataFolder();
		File schematicsDirectory = new File(dataDirectory, "schematics");

		if ((!schematicsDirectory.exists()) && (!schematicsDirectory.mkdirs())) {
			return;
		}

		File[] schematics = schematicsDirectory.listFiles();
		if (schematics == null) {
			return;
		}

		for (File schematic : schematics) {
			if (schematic.getName().endsWith(".schematic")) {

				if (!schematic.isFile()) {
					LogUtils.log(Level.INFO, getClass(), "Could not load schematic %s: Not a file",
							new Object[] { schematic.getName() });
				} else {
					SchematicFormat schematicFormat = SchematicFormat.getFormat(schematic);
					if (schematicFormat == null) {
						LogUtils.log(Level.INFO, getClass(),
								"Could not load schematic %s: Unable to determine schematic format",
								new Object[] { schematic.getName() });
					} else
						try {
							registerSchematic(schematic.getName().replace(".schematic", ""),
									schematicFormat.load(schematic));
						} catch (DataException e) {
							LogUtils.log(Level.INFO, getClass(), "Could not load schematic %s: %s",
									new Object[] { schematic.getName(), e.getMessage() });
						} catch (IOException e) {
							LogUtils.log(Level.INFO, getClass(), "Could not load schematic %s: %s",
									new Object[] { schematic.getName(), e.getMessage() });
						}
				}
			}
		}
		LogUtils.log(Level.INFO, getClass(), "Registered %d schematics ...",
				new Object[] { Integer.valueOf(this.schematicSize) });
	}

	public Collection<CuboidClipboard> getAllSchematics() {
		return schematicMap.values();
	}

	private void registerSchematic(final String name, final CuboidClipboard schematic) {
		org.bukkit.Bukkit.getScheduler().runTaskAsynchronously(EggWars.get(), new Runnable() {

			public void run() {
				int spawnId = 0;
				Vector[] locations = new Vector[9];
				int villagerId = 0;
				Vector[] villagers = new Vector[5];
				List<Vector> generators = Lists.newArrayList();

				for (int y = 0; y < schematic.getSize().getBlockY(); y++) {
					for (int x = 0; x < schematic.getSize().getBlockX(); x++) {
						for (int z = 0; z < schematic.getSize().getBlockZ(); z++) {
							Vector currentPoint = new Vector(x, y, z);
							int currentBlock = schematic.getPoint(currentPoint).getType();
							if (currentBlock != 0) {
								if ((currentBlock == Material.BEACON.getId())) {
									Vector loc = currentPoint;
									if (schematic.getBlock(loc.add(0, 1, 0)).getId() == 35) {
										if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 11) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[0] = currentPoint;
											spawnId++;
										} else if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 14) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[1] = currentPoint;
											spawnId++;
										} else if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 4) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[2] = currentPoint;
											spawnId++;
										} else if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 5) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[3] = currentPoint;
											spawnId++;
										} else if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 0) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[4] = currentPoint;
											spawnId++;
										}
									} else if (schematic.getBlock(loc.add(0, 1, 0)).getId() == 25) {
										villagers[villagerId++] = currentPoint;
										schematic.getBlock(loc).setId(0);
										schematic.getBlock(loc.add(0, 1, 0)).setId(0);
									}
								} else if ((currentBlock == Material.JUKEBOX.getId())) {
									Vector loc = currentPoint;
									if (schematic.getBlock(loc.add(0, 1, 0)).getId() == 35) {
										if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 11) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[5] = currentPoint;
											spawnId++;
										} else if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 14) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[6] = currentPoint;
											spawnId++;
										} else if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 4) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[7] = currentPoint;
											spawnId++;
										} else if (schematic.getBlock(loc.add(0, 1, 0)).getData() == 5) {
											schematic.getBlock(loc).setId(0);
											schematic.getBlock(loc.add(0, 1, 0)).setId(0);

											locations[8] = currentPoint;
											spawnId++;
										}
									}
								}else if(currentBlock == Material.IRON_BLOCK.getId()){
									Vector loc = currentPoint;
									if (schematic.getBlock(loc.add(0, 1, 0)).getId() == Material.WALL_SIGN.getId()) {
										generators.add(currentPoint);
									}
								}else if(currentBlock == Material.GOLD_BLOCK.getId()){
									Vector loc = currentPoint;
									if (schematic.getBlock(loc.add(0, 1, 0)).getId() == Material.WALL_SIGN.getId()) {
										generators.add(currentPoint);
									}
								}else if(currentBlock == Material.DIAMOND_BLOCK.getId()){
									Vector loc = currentPoint;
									if (schematic.getBlock(loc.add(0, 1, 0)).getId() == Material.WALL_SIGN.getId()) {
										generators.add(currentPoint);
									}
								}
							}
						}
					}
				}
				if (spawnId < 9) {
					SchematicController.this.noSpawnsNotifier(name);
					return;
				}
				SchematicController.get().cacheSpawn(schematic, locations);
				SchematicController.get().cacheVillagers(schematic, villagers);
				SchematicController.get().cacheGenerators(schematic, generators);
				SchematicController.this.schematicMap.put(name, schematic);
			}

		});
		this.schematicSize += 1;
	}

	public CuboidClipboard getMap(String mapName) {
		List<CuboidClipboard> schematics = Lists.newArrayList(this.schematicMap.values());
		for (String a : this.schematicMap.keySet()) {
			if (a.equalsIgnoreCase(mapName)) {
				return this.schematicMap.get(a);
			}
		}
		return (CuboidClipboard) schematics.get(this.random.nextInt(schematics.size()));
	}

	public CuboidClipboard getMapNull(String mapName) {
		if (this.schematicMap.containsKey(mapName)) {
			return this.schematicMap.get(mapName);
		}
		return null;
	}

	public String getName(CuboidClipboard cuboidClipboard) {
		for (Map.Entry<String, CuboidClipboard> entry : this.schematicMap.entrySet()) {
			if (((CuboidClipboard) entry.getValue()).equals(cuboidClipboard)) {
				return (String) entry.getKey();
			}
		}

		return null;
	}

	public void cacheSpawn(CuboidClipboard schematic, Vector[] locations) {
		this.spawnCache.put(schematic, locations);
	}

	public void cacheVillagers(CuboidClipboard schematic, Vector[] locations) {
		this.villagerCache.put(schematic, locations);
	}
	
	public void cacheGenerators(CuboidClipboard schematic, List<Vector> locations) {
		this.generatorCache.put(schematic, locations);
	}

	public void noSpawnsNotifier(String name) {
		System.out.println("[" + EggWars.getGameAlias() + "] [SchematicController] Schematic '" + name
				+ "' doesn't have all spawns!");
	}

	public Vector[] getCachedSpawns(CuboidClipboard schematic) {
		return this.spawnCache.get(schematic);
	}

	public Vector[] getCachedVillagers(CuboidClipboard schematic) {
		return this.villagerCache.get(schematic);
	}
	
	public List<Vector> getCachedGenerator(CuboidClipboard schematic) {
		return this.generatorCache.get(schematic);
	}

	public int size() {
		return this.schematicMap.size();
	}

	public void remove(String schematic) {
		this.schematicMap.remove(schematic);
	}

	public static SchematicController get() {
		if (instance == null) {
			instance = new SchematicController();
		}
		return instance;
	}

}