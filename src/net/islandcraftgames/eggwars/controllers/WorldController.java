package net.islandcraftgames.eggwars.controllers;

import java.util.List;
import java.util.Queue;

import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;

import me.islandcraft.gameapi.IWEUtils;
import net.islandcraftgames.eggwars.EggWars;
import net.islandcraftgames.eggwars.config.PluginConfig;
import net.islandcraftgames.eggwars.game.Game;
import net.islandcraftgames.eggwars.generators.types.DiamondGenerator;
import net.islandcraftgames.eggwars.generators.types.GoldGenerator;
import net.islandcraftgames.eggwars.generators.types.IronGenerator;

@SuppressWarnings({ "deprecation", "unused" })
public class WorldController {
	private static final int PASTE_HEIGHT = 75;
	private static WorldController worldController;
	private World islandWorld;
	private final Queue<int[]> islandReferences = com.google.common.collect.Lists.newLinkedList();
	private int nextId;
	private int prevOffset = 0;

	public WorldController() {
		generateGridReferences();
		this.islandWorld = createWorld();
	}

	private void generateGridReferences() {
		for (int xxx = 0; xxx < PluginConfig.getIslandsPerWorld(); xxx += 10) {
			for (int zzz = 0; zzz < PluginConfig.getIslandsPerWorld(); zzz += 10) {
				int[] coordinates = { xxx, zzz };

				if (!this.islandReferences.contains(coordinates)) {
					this.islandReferences.add(coordinates);
				}
			}
		}
	}

	public World create(Game game, CuboidClipboard schematic) {
		if (this.islandReferences.size() == 0) {
			net.islandcraftgames.eggwars.utilities.LogUtils.log(java.util.logging.Level.INFO, getClass(),
					"No more free islands left. Generating new world.", new Object[0]);

			generateGridReferences();
			this.islandWorld = createWorld();
		}

		int[] gridReference = (int[]) this.islandReferences.poll();
		game.setGridReference(gridReference);

		int gridX = gridReference[0];
		int gridZ = gridReference[1];
		int length = schematic.getLength();
		int width = schematic.getWidth();
		int islandSize = length > width ? length : width;
		int offsetX = schematic.getOffset().getBlockX();
		int offsetZ = schematic.getOffset().getBlockZ();
		int offset = offsetX < offsetZ ? offsetX : offsetZ;
		int buffer = PluginConfig.getIslandBuffer();

		int midX = gridX * (org.bukkit.Bukkit.getViewDistance() * 16 + 15 - offset + this.prevOffset + buffer * 2);
		int midZ = gridZ * (org.bukkit.Bukkit.getViewDistance() * 16 + 15 - offset + this.prevOffset + buffer * 2);

		game.setLocation(midX, midZ);

		this.prevOffset = (islandSize / 2);

		Vector[] spawns = SchematicController.get().getCachedSpawns(schematic);
		Vector[] villagers = SchematicController.get().getCachedVillagers(schematic);
		List<Vector> generators = SchematicController.get().getCachedGenerator(schematic);
		Vector mapLoc = new Vector(midX, 100, midZ);
		Location[] spawnsNew = new Location[spawns.length];

		for (int i = 0; i < spawns.length; i++) {
			Vector spawn = (spawns[i]).add(mapLoc).add(schematic.getOffset());
			spawnsNew[i] = new Location(this.islandWorld, spawn.getBlockX() + 0.5D, spawn.getBlockY() + 0.5D,
					spawn.getBlockZ() + 0.5D);
		}
		for (int i = 0; i < villagers.length; i++) {
			Vector villager = (villagers[i]).add(mapLoc).add(schematic.getOffset());
			game.addVillager(new Location(this.islandWorld, villager.getBlockX() + 0.5D, villager.getBlockY() + 0.5D,
					villager.getBlockZ() + 0.5D));
		}
		for (Vector v : generators) {
			Vector generatorV = v.add(mapLoc).add(schematic.getOffset());
			game.addGenerator(new Location(this.islandWorld, generatorV.getBlockX() + 0.5D,
					generatorV.getBlockY() + 0.5D, generatorV.getBlockZ() + 0.5D));

		}

		game.addSpawns(spawnsNew);

		new IWEUtils(game, new Location(this.islandWorld, midX, 100.0D, midZ), schematic);

		return this.islandWorld;
	}

	private World createWorld() {
		String worldName = EggWars.getGameAlias() + "-" + getNextId();
		World world = null;
		MultiverseCore mV = (MultiverseCore) EggWars.get().getServer().getPluginManager().getPlugin("Multiverse-Core");
		if (mV != null) {
			if (mV.getMVWorldManager().loadWorld(worldName)) {
				return mV.getMVWorldManager().getMVWorld(worldName).getCBWorld();
			}
			Boolean ret = Boolean.valueOf(mV.getMVWorldManager().addWorld(worldName, World.Environment.NORMAL, null,
					org.bukkit.WorldType.NORMAL, Boolean.valueOf(false), EggWars.getPluginName(), false));

			if (ret.booleanValue()) {
				MultiverseWorld mvWorld = mV.getMVWorldManager().getMVWorld(worldName);
				world = mvWorld.getCBWorld();
				mvWorld.setDifficulty(Difficulty.PEACEFUL);
				mvWorld.setPVPMode(true);
				mvWorld.setEnableWeather(false);
				mvWorld.setKeepSpawnInMemory(false);
				mvWorld.setAllowAnimalSpawn(false);
				mvWorld.setAllowMonsterSpawn(false);
			}
		}
		if (world == null) {
			WorldCreator worldCreator = new WorldCreator(worldName);
			worldCreator.environment(World.Environment.NORMAL);
			worldCreator.generateStructures(false);
			worldCreator.generator(EggWars.getPluginName());
			world = worldCreator.createWorld();
			world.setDifficulty(Difficulty.PEACEFUL);
			world.setSpawnFlags(false, false);
			world.setPVP(true);
			world.setStorm(false);
			world.setThundering(false);
			world.setWeatherDuration(Integer.MAX_VALUE);
			world.setKeepSpawnInMemory(false);
			world.setTicksPerAnimalSpawns(0);
			world.setTicksPerMonsterSpawns(0);
		}
		world.setAutoSave(false);
		world.setGameRuleValue("doFireTick", "false");

		return world;
	}

	private int getNextId() {
		int id = this.nextId++;

		if (this.nextId == Integer.MAX_VALUE) {
			this.nextId = 0;
		}

		return id;
	}

	public static WorldController get() {
		if (worldController == null) {
			worldController = new WorldController();
		}

		return worldController;
	}

}