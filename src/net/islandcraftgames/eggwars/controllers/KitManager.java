package net.islandcraftgames.eggwars.controllers;

import java.util.List;

import me.islandcraft.gameapi.IKit;
import net.islandcraftgames.eggwars.config.PluginConfig;
import net.islandcraftgames.eggwars.kits.KitsMain;
import net.islandcraftgames.eggwars.player.GamePlayer;

public class KitManager {

	public static void getKits(GamePlayer gp) {
		if (!PluginConfig.isKitsOn())
			return;
		List<IKit> a = KitsMain.getAllKits();
		boolean[] kits = gp.getKits();
		for (IKit kit : a) {
			if (kits[kit.getID()]) {
				gp.getBukkitPlayer().getInventory().addItem(kit.getItems());
			}
		}
	}
}
