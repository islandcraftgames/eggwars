package net.islandcraftgames.eggwars.controllers;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.collect.Lists;
import com.sk89q.worldedit.CuboidClipboard;

import me.islandcraft.gameapi.IGame;
import me.islandcraft.gameapi.IGameController;
import me.islandcraft.gameapi.IGameState;
import net.islandcraftgames.eggwars.game.Game;

@SuppressWarnings("deprecation")
public class GameController extends IGameController{
	private final int minArenasAmount = 1;
	private static GameController instance;
	private List<IGame> gameList = Lists.newArrayList();
	private static boolean disabled = false;

	public Game findEmpty(String mapName, Integer partySize) {
		for (IGame game : this.gameList) {
			if ((game.getState() != IGameState.PLAYING) && (!game.isFull())
					&& (game.getSlots() - game.getPlayerCount()) >= partySize) {
				if (SchematicController.get().getName(game.getSchematic())
						.equalsIgnoreCase(mapName))
					return (Game) game;
			}
		}

		if (SchematicController.get().getMapNull(mapName) != null) {
			return create(mapName);
		}

		for (IGame game : this.gameList) {
			if ((game.getState() != IGameState.PLAYING) && (!game.isFull())
					&& (game.getSlots() - game.getPlayerCount()) >= partySize) {
				return (Game) game;
			}
		}

		return create(mapName);
	}

	public int getPlayersWaiting(String name) {
		int pWaiting = 0;

		if (SchematicController.get().getMapNull(name) != null) {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (SchematicController.get().getName(game.getSchematic())
						.equalsIgnoreCase(name))
					if (game.getState() == IGameState.WAITING) {
						pWaiting = pWaiting + game.getPlayerCount();
					}
			}
		} else {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (game.getState() == IGameState.WAITING) {
					pWaiting = pWaiting + game.getPlayerCount();
				}
			}
		}

		return pWaiting;
	}

	public int getPlayersPlaying(String name) {
		int pWaiting = 0;

		if (SchematicController.get().getMapNull(name) != null) {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (SchematicController.get().getName(game.getSchematic())
						.equalsIgnoreCase(name))
					if (game.getState() == IGameState.PLAYING) {
						pWaiting = pWaiting + game.getPlayerCount();
					}
			}
		} else {
			if (this.gameList == null || this.gameList.size() == 0)
				return 0;
			for (IGame game : this.gameList) {
				if (game.getState() == IGameState.PLAYING) {
					pWaiting = pWaiting + game.getPlayerCount();
				}
			}
		}

		return pWaiting;
	}

	public Game create(String mapName) {
		CuboidClipboard schematic = SchematicController.get().getMap(mapName);
		Game game = new Game(schematic);
		this.gameList.add(game);
		sendUpdate();
		return game;
	}

	public void mantainArenas() {
		for (CuboidClipboard s : SchematicController.get().getAllSchematics()) {
			if (maintainSchematic(s))
				return;
		}
		System.out.println("Finished configuring all arenas");
	}

	private boolean maintainSchematic(CuboidClipboard schematic) {
		int amount = 0;
		for (IGame game : getAll()) {
			if (game.getSchematic() == schematic
					&& game.getState() == IGameState.WAITING)
				amount++;
		}
		if (amount < this.minArenasAmount) {
			this.gameList.add(new Game(schematic));
			System.out.println("Stating configuring for arena "
					+ SchematicController.get().getName(schematic));
			return true;
		}
		return false;
	}

	public void remove(@Nonnull Game game) {
		this.gameList.remove(game);
	}

	public void shutdown() {
		if (gameList.size() == 0)
			return;
		for (IGame game : this.gameList) {
			game.onGameEnd();
		}
	}

	public Collection<IGame> getAll() {
		return this.gameList;
	}

	public static GameController get() {
		if (instance == null) {
			return instance = new GameController();
		}

		return instance;
	}
	
	public boolean canRestart() {
		for (IGame game : getAll()) {
			if (game.getPlayerCount() > 0)
				return false;
		}
		return true;
	}

	public void disableGames() {
		disabled = true;
	}

	public boolean isDisabled() {
		return disabled;
	}

}