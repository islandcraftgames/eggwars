package net.islandcraftgames.eggwars.controllers;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class RandomController {

	public static ItemStack randomItem() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.COBBLESTONE);
		case 2:
			return new ItemStack(Material.DIRT);
		case 3:
			return new ItemStack(Material.GRASS);
		case 4:
			return new ItemStack(Material.GRAVEL);
		case 5:
			return new ItemStack(Material.SAND);
		default:
			return new ItemStack(Material.AIR);
		}
	}
}
